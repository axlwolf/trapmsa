<tr>
                                <td colspan="4">
                                    <strong style="font-size:25px;">Opcionales</strong>
                                </td>
                            </tr>


                            <tr class="item-row">
                            <td class="item-name">
                                <div>
                                    <strong>Patin Extra</strong>
                                </div>
                            </td>

                            <td>
                                <input type="text" class="cost opc" id="patin_ext" name="patin_ext" placeholder="$0.00"/>
                            </td>

                            <td>
                                <input type="text" class="price iv" id="precio_u1" name="precio_u1" disabled readonly placeholder="$0.00"/>
                            </td>
                            <td>
                                <input type="text" class="iva" id="total" name="total" disabled readonly placeholder="$0.00"/>
                            </td>
                        </tr>

                        <tr class="item-row">
                            <td class="item-name">
                                <div >
                                    <strong>Retractil 1 eje</strong>
                                </div>
                            </td>

                            <td>
                                <input type="text" class="cost opc" id="retractil_1" name="retractil_1" placeholder="$0.00"/>
                            </td>

                            <td>
                                <input type="text" class="price iv" id="precio_u2" name="precio_u2" disabled readonly placeholder="$0.00"/>
                            </td>

                            <td>
                                <input type="text" class="iva" id="total1" name="total1" disabled readonly placeholder="$0.00"/>
                            </td>
                        </tr>
                        <tr class="item-row">
                            <td class="item-name">
                                <div >
                                    <strong>Retractil 2 ejes</strong>
                                </div>
                            </td>

                            <td>
                                <input type="text" class="cost opc" id="retractil_2" name="retractil_2" placeholder="$0.00"/>
                            </td>

                            <td>
                                <input type="text" class="price iv" id="precio_u3" name="precio_u3" disabled readonly placeholder="$0.00"/>
                            </td>

                            <td>
                                <input type="text" class="iva" id="total2" name="total2" disabled readonly placeholder="$0.00"/>
                            </td>
                        </tr>

                        <tr class="item-row">
                            <td class="item-name">
                                <div >
                                    <strong>Caja de herramientas</strong>
                                </div>
                            </td>

                            <td>
                                <input type="text" class="cost opc" id="caja_h" name="caja_h" placeholder="$0.00"/>
                            </td>

                            <td>
                                <input type="text" class="price iv" id="precio_u4" name="precio_u4" disabled readonly placeholder="$0.00"/>
                            </td>

                            <td>
                                <input type="text" class="iva" id="total3" name="total3" disabled readonly placeholder="$0.00"/>
                            </td>
                        </tr>

                        <tr class="item-row">
                            <td class="item-name">
                                <div >
                                    <strong>Gancho de arrastre PH-400 Holland</strong>
                                </div>
                            </td>

                            <td>
                                <input type="text" class="cost opc" id="gancho_holland" name="gancho_holland" placeholder="$0.00"/>
                            </td>

                            <td>
                                <input type="text" class="price iv" id="precio_u5" name="precio_u5" disabled readonly placeholder="$0.00"/>
                            </td>

                            <td>
                                <input type="text" class="iva" id="total4" name="total4" disabled readonly placeholder="$0.00"/>
                            </td>
                        </tr>

                        <tr class="item-row">
                            <td class="item-name">
                                <div >
                                    <strong>Autonflado 2 ejes</strong>
                                </div>
                            </td>

                            <td>
                                <input type="text" class="cost opc" id="autoinflado_2" name="autoinflado_2" placeholder="$0.00"/>
                            </td>

                            <td>
                                <input type="text" class="price iv" id="precio_u6" name="precio_u6" disabled readonly placeholder="$0.00"/>
                            </td>

                            <td>
                                <input type="text" class="iva" id="total5" name="total5" disabled readonly placeholder="$0.00"/>
                            </td>
                        </tr>
                        <tr class="item-row">
                            <td class="item-name">
                                <div >
                                    <strong>Autonflado 3 ejes</strong>
                                </div>
                            </td>

                            <td>
                                <input type="text" class="cost opc" id="autoinflado_3" name="autoinflado_3" placeholder="$0.00"/>
                            </td>

                            <td>
                                <input type="text" class="price iv" id="precio_u7" name="precio_u7" disabled readonly placeholder="$0.00"/>
                            </td>

                            <td>
                                <input type="text" class="iva" id="total6" name="total6" disabled readonly placeholder="$0.00"/>
                            </td>
                        </tr>
                            <?php
                            //Opcioales Caja seca
                            if($_GET['id'] == 4) {
                                ?>

                                <tr class="item-row">
                                    <td class="item-name">
                                        <div >
                                            <strong>Puerta Cortina</strong>
                                        </div>
                                    </td>

                                    <td>
                                        <input type="text" class="cost opc" id="puerta_cortina" name="puerta_cortina" placeholder="$0.00"/>
                                    </td>

                                    <td>
                                        <input type="text" class="price iv" id="precio_u8" name="precio_u8" disabled readonly placeholder="$0.00"/>
                                    </td>

                                    <td>
                                        <input type="text" class="iva" id="total7" name="total7" disabled readonly placeholder="$0.00"/>
                                    </td>
                                </tr>
                                <tr class="item-row">
                                    <td class="item-name">
                                        <div >
                                            <strong>Ventilas</strong>
                                        </div>
                                    </td>

                                    <td>
                                        <input type="text" class="cost opc" id="ventilas" name="ventilas" placeholder="$0.00"/>
                                    </td>

                                    <td>
                                        <input type="text" class="price iv" id="precio_u9" name="precio_u9" disabled readonly placeholder="$0.00"/>
                                    </td>

                                    <td>
                                        <input type="text" class="iva" id="total8" name="total8" disabled readonly placeholder="$0.00"/>
                                    </td>
                                </tr>

                                <tr class="item-row">
                                    <td class="item-name">
                                        <div >
                                            <strong>Puerta lateral</strong>
                                        </div>
                                    </td>

                                    <td>
                                        <input type="text" class="cost opc" id="puerta _latetal" name="puerta_lateral" placeholder="$0.00"/>
                                    </td>

                                    <td>
                                        <input type="text" class="price iv" id="precio_u11" name="precio_u11" disabled readonly placeholder="$0.00"/>
                                    </td>

                                    <td>
                                        <input type="text" class="iva" id="total10" name="total10" disabled readonly placeholder="$0.00"/>
                                    </td>
                                </tr>
                                <tr class="item-row">
                                    <td class="item-name">
                                        <div >
                                            <strong>Interior linner</strong>
                                        </div>
                                    </td>

                                    <td>
                                        <input type="text" class="cost opc" id="interior_linner" name="interior_linner" placeholder="$0.00"/>
                                    </td>

                                    <td>
                                        <input type="text" class="price iv" id="precio_u12" name="precio_u12" disabled readonly placeholder="$0.00"/>
                                    </td>

                                    <td>
                                        <input type="text" class="iva" id="total11" name="total11" disabled readonly placeholder="$0.00"/>
                                    </td>
                                </tr>
                                <tr class="item-row">
                                    <td class="item-name">
                                        <div >
                                            <strong>Sistema antivolcadura</strong>
                                        </div>
                                    </td>

                                    <td>
                                        <input type="text" class="cost opc" id="antivolcadura" name="antivolcadura" placeholder="$0.00"/>
                                    </td>

                                    <td>
                                        <input type="text" class="price iv" id="precio_u13" name="precio_u13" disabled readonly placeholder="$0.00"/>
                                    </td>

                                    <td>
                                        <input type="text" class="iva" id="total12" name="total12" disabled readonly placeholder="$0.00"/>
                                    </td>
                                </tr>
                            <?php
                            }

                            ?>
                            <tr class="item-row">
                                <td class="item-name">
                                    <div >
                                        <strong>Piso antiderrapante</strong>
                                    </div>
                                </td>

                                <td>
                                    <input type="text" class="cost opc" id="piso_antiderrapante" name="piso_antiderrapante" placeholder="$0.00"/>
                                </td>

                                <td>
                                    <input type="text" class="price iv" id="precio_u10" name="precio_u10" disabled readonly placeholder="$0.00"/>
                                </td>

                                <td>
                                    <input type="text" class="iva" id="total9" name="total9" disabled readonly placeholder="$0.00"/>
                                </td>
                            </tr>
                            <tr class="item-row">
                                <td class="item-name">
                                    <div >
                                        <strong>Winches</strong>
                                    </div>
                                </td>

                                <td>
                                    <input type="text" class="cost opc" id="winches" name="winches" placeholder="$0.00"/>
                                </td>

                                <td>
                                    <input type="text" class="price iv" id="precio_u14" name="precio_u14" disabled readonly placeholder="$0.00"/>
                                </td>

                                <td>
                                    <input type="text" class="iva" id="total9" name="total13" disabled readonly placeholder="$0.00"/>
                                </td>
                            </tr>