<?php
session_start();

include "funciones/funciones.php";

?>

<!doctype html>
<html lang="es">
	<head>
		<meta charset="UTF-8">
		<title>Cotizador TRAPMSA</title>
		<link href="css/bootstrap.min.css" rel="stylesheet">
		<link rel="stylesheet" href="css/material.css"/>
		<link rel="stylesheet" href="css/ripples.css"/>
		<link rel="stylesheet" href="css/material-wfont.css"/>
		<link rel="stylesheet" href="css/estilo1.css"/>

	</head>
	<body>
		<div class="container principal" class="copy-html">
			<!-- <form id="formulario" action="#"> -->
			<!-- <form id="formulario" action="indexc.php" method="post"> -->
            <div id="formulario">
				<div class="row">
					<div class="container">
						<header class="page-headera">
							<div class="col-md-10 col-md-pull-0">
								<img src="img/cotizador_header.png" alt="header"/>
							</div>
						</header>
					</div>
				</div>

				<div class="row">
					<div class="container">
						<div class="col-md-offset-1 col-md-10">
							<section id="html1">
								<p>
									<label for="fecha"> FECHA:</label>
									<input type="text" class="input-xlarge " readonly="readonly" name="fecha" id="fecha" value="<?= date("d-m-Y") ?>" />
								</p>
								<p>
									<label for="contacto"> CONTACTO:</label>
									<input type="text" class="input-xlarge"  name="contacto" id="contacto" />
								</p>
								<p>
									<label for="email"> Email:</label>
									<input type="text" class="input-xlarge" name="email" id="email" />
								</p>
							</section>
							<div id="errores">

							</div>
						</div>
					</div>
				</div>
				<div class="row">
                    <section id="html2">
					<h4 class="titulo">
                        <label for="folio">COTIZACION</label> <input class="input-xlarge" type="text" name="folio" id="folio" readonly="readonly" value="<?= $invoice ?>"/>
                        <input class="input-xlarge" type="hidden" id="tipo_cotiza" name="tipo_cotiza" readonly value="<?= id_cotiza($_GET['id']); ?>"/>
                    </h4>

					<section>
						<p class="titulo completo">
							De antemano agradecemos tu preferencia y le presento la cotización de la unidad posteriormente descrita.
						</p>
					</section>
                </section>
    
					<h4 class="titulo"> DESCRIPCIÓN </h4>
					<div class="col-md-offset-1 col-md-10">


                        <?php

                            if($_GET['id']== 1){

                            ?>
                                <table class="table table-striped table-hover table-bordered">
                                    <thead>
                                    <tr>
                                        <th>TIPO</th>
                                        <th>MODELO</th>
                                        <th>AÑO</th>
                                        <th>LARGO EXT</th>
                                        <th>ANCHO EXT</th>
                                        <th>ALTO T</th>
                                        <th>EJES</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <th>
                                            <select name="volteo" id="volteo" class="volteo">
                                                <option value="VOLTEO ACERO AL CARBON 24mts3">VOLTEO ACERO AL CARBON 24mts3</option>
                                                <option value="VOLTEO ACERO INOX 24mts3">VOLTEO ACERO INOX 24mts3</option>
                                                <option value="VOLTEO HARDOX 24mts3">VOLTEO HARDOX 24mts3</option>
                                                <option value="VOLTEO ACERO AL CARBON 30mts3" class="30mts">VOLTEO ACERO AL CARBON
                                                    30mts3
                                                </option>
                                                <option value="VOLTEO ACERO INOX 30mts3" class="30mts">VOLTEO ACERO INOX
                                                    30mts3
                                                </option>
                                                <option value="VOLTEO HARDOX 30mts3" class="30mts">VOLTEO HARDOX 30mts3
                                                </option>
                                            </select></th>
                                        <th>
                                            <select name="modelo" id="modelo" class="modelo">
                                                <option value="TPM-AF-HENDRICKSON">TPM-AF-HENDRICKSON</option>
                                                <option value="TPM-AF-NACIONAL">TPM-AF-NACIONAL</option>
                                            </select></th>
                                        <th>
                                            <?php $fechaValor=date("Y");?>
                                            <select id="dates">
                                                <?php
                                                for($i=2015;$i<=2025;$i++){
                                                    ?><option><?php echo $fechaValor;?></option>
                                                    <?php
                                                    $fechaValor=$fechaValor+1;
                                                }?>
                                            </select>
                                        </th>
                                        <th>
                                            <select name="largo" id="largo" class="largo_volteo" >
                                                <option value="7.31mts" class="largo">7.31mts</option>
                                                <option value="9.144mts" class="largo">9.144mts</option>
                                            </select>
                                        </th>
                                        <th id="ancho_ext">2.60mts</th>
                                        <th>
                                            <select name="alto" id="alto">
                                                <option value="2.77mts">2.77 mts</option>
                                                <option value="3.07mts">3.07 mts</option>
                                            </select>
                                        </th>
                                        <th>
                                            <select name="ejes" id="ejes" class="ejes">
                                                <option value="2" class="no_ejes">2</option>
                                                <option value="3" class="no_ejes">3</option>
                                            </select></th>
                                    </tr>
                                    </tbody>
                                </table>

                        <?php
                                //Datos de Tolva
                        }elseif ($_GET['id']== 2) {
                                ?>

                                <table class="table table-striped table-hover table-bordered">
                                    <thead>
                                    <tr>
                                        <th>TIPO</th>
                                        <th>MODELO</th>
                                        <th>AÑO</th>
                                        <th>LARGO EXT</th>
                                        <th>ANCHO EXT</th>
                                        <th>ALTO T</th>
                                        <th>EJES</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <th>
                                            <select name="volteo" id="volteo">
                                                <option value="TOLVA PRESURIZADA 28 MTS3 2 EJES" selected>  TOLVA PRESURIZADA 28 MTS3 2 EJES</option>
                                                <option value="TOLVA PRESURIZADA 36 MTS3 3 EJES">  TOLVA PRESURIZADA 36 MTS3 3 EJES</option>
                                            </select>
                                        </th>
                                        <th>
                                            <select name="modelo" id="modelo" class="modelo">
                                                <option value="TPM-AF-HENDRICKSON">TPM-AF-HENDRICKSON</option>
                                                <option value="TPM-AF-NACIONAL">TPM-AF-NACIONAL</option>
                                            </select></th>
                                        <th>
                                            <?php $fechaValor=date("Y");?>
                                            <select id="dates">
                                                <?php
                                                for($i=2015;$i<=2025;$i++){
                                                    ?><option><?php echo $fechaValor;?></option>
                                                    <?php
                                                    $fechaValor=$fechaValor+1;
                                                }?>
                                            </select>
                                        </th>
                                        <th id="largo">8.95 mts</th>
                                        <th id="ancho_ext">2.60mts</th>
                                        <th>
                                            <select name="alto" id="alto">
                                                <option value="3.90 mts">3.90 mts</option>
                                            </select>
                                        </th>
                                        <th>
                                            <select name="ejes" id="ejes" class="ejes">
                                                <option value="2" class="no_ejes">2</option>
                                                <option value="3" class="no_ejes">3</option>
                                                <!--                                            <option value="3" class="no_ejes">3</option>-->
                                            </select></th>
                                    </tr>
                                    </tbody>
                                </table>

                            <?php
                                //Datos de dolly
                            }elseif($_GET['id'] == 3) {
                                ?>

                                <table class="table table-striped table-hover table-bordered">
                                    <thead>
                                    <tr>
                                        <th>TIPO</th>
                                        <th>MODELO</th>
                                        <th>AÑO</th>
                                        <th>LARGO EXT</th>
                                        <th>ANCHO EXT</th>
                                        <th>ALTO T</th>
                                        <th>EJES</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <th>
                                            <select name="volteo" id="volteo">
                                                <option value="DOLLY" selected>DOLLY</option>
                                            </select>
                                        </th>
                                        <th>
                                            <select name="modelo" id="modelo" class="modelo">
                                                <option value="TPM-AF-NACIONAL">TPM-AF-NACIONAL</option>
                                                <option value="TPM-AF-HENDRICKSON">TPM-AF-HENDRICKSON</option>
                                            </select></th>
                                        <th>
                                            <?php $fechaValor=date("Y");?>
                                            <select id="dates">
                                                <?php
                                                for($i=2015;$i<=2025;$i++){
                                                    ?><option><?php echo $fechaValor;?></option>
                                                    <?php
                                                    $fechaValor=$fechaValor+1;
                                                }?>
                                            </select>
                                        </th>
                                        <th id="largo">3.54mts</th>
                                        <th id="ancho_ext">2.60mts</th>
                                        <th>
                                            <select name="alto" id="alto">
                                                <option value="1.28mts">1.28 mts</option>
                                            </select>
                                        </th>
                                        <th>
                                            <select name="ejes" id="ejes" class="ejes">
                                                <option value="2" class="no_ejes">2</option>
                                                <!--                                            <option value="3" class="no_ejes">3</option>-->
                                            </select></th>
                                    </tr>
                                    </tbody>
                                </table>
                            <?php
                                //Datos de Caja seca
                            }elseif($_GET['id']==4) {
                                ?>
                            <table class="table table-striped table-hover table-bordered">
                                <thead>
                                <tr>
                                    <th>TIPO</th>
                                    <th>MODELO</th>
                                    <th>AÑO</th>
                                    <th>LARGO EXT</th>
                                    <th>ANCHO EXT</th>
                                    <th>ALTO T</th>
                                    <th>EJES</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <th>
                                        <select name="volteo" id="volteo" class="caja_seca" onChange="caja();">
                                            <option value="CAJA SECA 28'" selected>CAJA SECA 28'</option>
                                            <option value="CAJA SECA 40'"> CAJA SECA 40' </option>
                                            <option value="CAJA SECA 48'"> CAJA SECA 48' </option>
                                            <option value="CAJA SECA 53'"> CAJA SECA 53' </option>
                                        </select>
                                    </th>
                                    <th>
                                        <select name="modelo" id="modelo" class="modelo">
                                            <option value="TPM-AF-NACIONAL">TPM-AF-NACIONAL</option>
                                            <option value="TPM-AF-HENDRICKSON">TPM-AF-HENDRICKSON</option>
                                        </select></th>
                                    <th>
                                        <?php $fechaValor=date("Y");?>
                                            <select id="dates">
                                                <?php
                                                for($i=2015;$i<=2025;$i++){
                                                    ?><option><?php echo $fechaValor;?></option>
                                                    <?php
                                                    $fechaValor=$fechaValor+1;
                                                }?>
                                            </select>
                                    </th>
                                    <th id="largo">8.53 MTS
                                    </th>
                                    <th id="ancho_ext">2.60mts</th>
                                    <th>
                                        <select name="alto" id="alto">
                                            <option value="4.11mts">4.11mts</option>
                                        </select>
                                    </th>
                                    <th>
                                        <select name="ejes" id="ejes" class="ejes">
                                            <option value="1" class="no_ejes">1</option>
                                            <option value="2" class="no_ejes">2</option>
                                            <option value="3" class="no_ejes">3</option>
                                        </select></th>
                                </tr>
                                </tbody>
                            </table>

                            <?php
                            }elseif($_GET['id'] == 5) {
                                ?>
                                <table class="table table-striped table-hover table-bordered">
                                    <thead>
                                    <tr>
                                        <th>TIPO</th>
                                        <th>MODELO</th>
                                        <th>AÑO</th>
                                        <th>LARGO EXT</th>
                                        <th>ANCHO EXT</th>
                                        <th>ALTO T</th>
                                        <th>EJES</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <th>
                                            <select name="volteo" id="volteo" class="plataforma">
                                                <option value="PLATAFORMA DE 26'" selected> PLATAFORMA DE 26' </option>
                                                <option value="PLATAFORMA DE 30'" > PLATAFORMA DE 30' </option>
                                                <option value="PLATAFORMA DE 35'" > PLATAFORMA DE 35' </option>
                                                <option value="PLATAFORMA DE 38'" > PLATAFORMA DE 38' </option>
                                                <option value="PLATAFORMA DE 39'" > PLATAFORMA DE 39' </option>
                                                <option value="PLATAFORMA DE 40'" > PLATAFORMA DE 40' </option>
                                                <option value="PLATAFORMA DE 42'" > PLATAFORMA DE 42' </option>
                                                <option value="PLATAFORMA DE 45'" > PLATAFORMA DE 45' </option>
                                                <option value="PLATAFORMA DE 48'" > PLATAFORMA DE 48' </option>
                                            </select>
                                        </th>
                                        <th>
                                            <select name="modelo" id="modelo" class="modelo">
                                                <option value="TPM-AF-NACIONAL">TPM-AF-NACIONAL</option>
                                                <option value="TPM-AF-HENDRICKSON">TPM-AF-HENDRICKSON</option>
                                            </select>
                                        </th>
                                        <th>
                                            <?php $fechaValor=date("Y");?>
                                            <select id="dates">
                                                <?php
                                                for($i=2015;$i<=2025;$i++){
                                                    ?><option><?php echo $fechaValor;?></option>
                                                    <?php
                                                    $fechaValor=$fechaValor+1;
                                                }?>
                                            </select>
                                        </th>
                                        <th>
                                            <select name="largo" id="largo" class="largo-plataforma">
                                                <option value="7.92mts" class="largo">7.92mts</option>
                                                <option value="9.144mts" class="largo">9.144mts</option>
                                                <option value="10.67mts" class="largo">10.67mts</option>
                                                <option value="11.58mts" class="largo">11.58mts</option>
                                                <option value="11.88mts" class="largo">11.88mts</option>
                                                <option value="12.192mts" class="largo">12.192mts</option>
                                                <option value="12.80mts" class="largo">12.80mts</option>
                                                <option value="13.716mts" class="largo">13.716mts</option>
                                                <option value="14.63mts" class="largo">14.63mts</option>
                                            </select>
                                        </th>
                                        <th id="ancho_ext">2.60mts</th>
                                        <th>
                                            <select name="alto" id="alto">
                                                <option value="1.20mts">1.20mts</option>
                                            </select>
                                        </th>
                                        <th>
                                            <select name="ejes" id="ejes" class="ejes">
                                                <option value="2" class="no_ejes">2</option>
                                                <option value="3" class="no_ejes">3</option>
                                            </select></th>
                                    </tr>
                                    </tbody>
                                </table>
                            <?php
                            }elseif($_GET['id'] == 6){
                                 ?>
                                <table class="table table-striped table-hover table-bordered">
                                    <thead>
                                    <tr>
                                        <th>TIPO</th>
                                        <th>MODELO</th>
                                        <th>AÑO</th>
                                        <th>LARGO EXT</th>
                                        <th>ANCHO EXT</th>
                                        <th>ALTO T</th>
                                        <th>EJES</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <th>
                                            <select name="volteo" id="volteo" class="plataforma">
                                                <option value="Chasis R20'/40'" selected>Chasis R20'/40'</option>
                                            </select>
                                        </th>
                                        <th>
                                            <select name="modelo" id="modelo" class="modelo">
                                                <option value="TPM-AF-NACIONAL">TPM-AF-NACIONAL</option>
                                                <option value="TPM-AF-HENDRICKSON">TPM-AF-HENDRICKSON</option>
                                            </select>
                                        </th>
                                        <th>
                                            <?php $fechaValor=date("Y");?>
                                            <select id="dates">
                                                <?php
                                                for($i=2015;$i<=2025;$i++){
                                                    ?><option><?php echo $fechaValor;?></option>
                                                    <?php
                                                    $fechaValor=$fechaValor+1;
                                                }?>
                                            </select>
                                        </th>
                                        <th>
                                            <select name="largo" id="largo" class="largo-plataforma">
        
                                                <option value="12.192mts" class="largo">12.192mts</option>
                                                
                                            </select>
                                        </th>
                                        <th id="ancho_ext">2.60mts</th>
                                        <th>
                                            <select name="alto" id="alto">
                                                <option value="1.20mts">1.20mts</option>
                                            </select>
                                        </th>
                                        <th>
                                            <select name="ejes" id="ejes" class="ejes">
                                                <option value="2" class="no_ejes">2</option>
                                            </select></th>
                                    </tr>
                                    </tbody>
                                </table>
                            <?php
                        
                        }elseif($_GET['id'] == 7){
                          ?>
                                <table class="table table-striped table-hover table-bordered">
                                    <thead>
                                    <tr>
                                        <th>TIPO</th>
                                        <th>MODELO</th>
                                        <th>AÑO</th>
                                        <th>LARGO EXT</th>
                                        <th>ANCHO EXT</th>
                                        <th>ALTO T</th>
                                        <th>EJES</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <th>
                                            <select name="volteo" id="volteo" class="plataforma">
                                                <option value="Chasis EX20'/40'" selected>Chasis EX20'/40'</option>
                                            </select>
                                        </th>
                                        <th>
                                            <select name="modelo" id="modelo" class="modelo">
                                                <option value="TPM-AF-NACIONAL">TPM-AF-NACIONAL</option>
                                                <option value="TPM-AF-HENDRICKSON">TPM-AF-HENDRICKSON</option>
                                            </select>
                                        </th>
                                        <th>
                                            <?php $fechaValor=date("Y");?>
                                            <select id="dates">
                                                <?php
                                                for($i=2015;$i<=2025;$i++){
                                                    ?><option><?php echo $fechaValor;?></option>
                                                    <?php
                                                    $fechaValor=$fechaValor+1;
                                                }?>
                                            </select>
                                        </th>
                                        <th>
                                            <select name="largo" id="largo" class="largo-plataforma">
        
                                                <option value="12.192mts" class="largo">12.192mts</option>
                                                
                                            </select>
                                        </th>
                                        <th id="ancho_ext">2.60mts</th>
                                        <th>
                                            <select name="alto" id="alto">
                                                <option value="1.20mts">1.20mts</option>
                                            </select>
                                        </th>
                                        <th>
                                            <select name="ejes" id="ejes" class="ejes">
                                                <option value="2" class="no_ejes">2</option>
                                            </select></th>
                                    </tr>
                                    </tbody>
                                </table>
                            <?php
                        }elseif($_GET['id'] == 8) {
                                ?>
                                <table class="table table-striped table-hover table-bordered">
                                    <thead>
                                    <tr>
                                        <th>TIPO</th>
                                        <th>MODELO</th>
                                        <th>AÑO</th>
                                        <th>LARGO EXT</th>
                                        <th>ANCHO EXT</th>
                                        <th>ALTO T</th>
                                        <th>EJES</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <th>
                                            <select name="volteo" id="volteo" class="plataforma">
                                                <option value="PLAT. MULT. DE 40'" selected>PLAT. MULT. DE 40'</option>
                                            </select>
                                        </th>
                                        <th>
                                            <select name="modelo" id="modelo" class="modelo">
                                                <option value="TPM-AF-NACIONAL">TPM-AF-NACIONAL</option>
                                                <option value="TPM-AF-HENDRICKSON">TPM-AF-HENDRICKSON</option>
                                            </select>
                                        </th>
                                        <th>
                                            <?php $fechaValor=date("Y");?>
                                            <select id="dates">
                                                <?php
                                                for($i=2015;$i<=2025;$i++){
                                                    ?><option><?php echo $fechaValor;?></option>
                                                    <?php
                                                    $fechaValor=$fechaValor+1;
                                                }?>
                                            </select>
                                        </th>
                                        <th>
                                            <select name="largo" id="largo" class="largo-plataforma">
                                                <option value="12.192mts" class="largo">12.192mts</option>
                                            </select>
                                        </th>
                                        <th id="ancho_ext">2.60mts</th>
                                        <th>
                                            <select name="alto" id="alto">
                                                <option value="1.20mts">1.20mts</option>
                                            </select>
                                        </th>
                                        <th>
                                            <select name="ejes" id="ejes" class="ejes">
                                                <option value="2" class="no_ejes">2</option>
                                            </select></th>
                                    </tr>
                                    </tbody>
                                </table>
                            <?php
                            }elseif($_GET['id'] == 9) {
                               ?>
                                <table class="table table-striped table-hover table-bordered">
                                    <thead>
                                    <tr>
                                        <th>TIPO</th>
                                        <th>MODELO</th>
                                        <th>AÑO</th>
                                        <th>LARGO EXT</th>
                                        <th>ANCHO EXT</th>
                                        <th>ALTO T</th>
                                        <th>EJES</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <th>
                                            <select onChange="volteoPla()" name="volteo" id="volteo" class="volteoPla">
                                                <option value="Volteo - Plat.24mts3">Volteo - Plat.24mts3</option>
                                                <option value="Volteo - Plat.30mts3" class="30mts">Volteo - Plat.30mts3
                                            </select></th>
                                        <th>
                                            <select name="modelo" id="modelo" class="modelo">
                                                <option value="TPM-AF-HENDRICKSON">TPM-AF-HENDRICKSON</option>
                                                <option value="TPM-AF-NACIONAL">TPM-AF-NACIONAL</option>
                                            </select></th>
                                        <th>
                                            <?php $fechaValor=date("Y");?>
                                            <select id="dates">
                                                <?php
                                                for($i=2015;$i<=2025;$i++){
                                                    ?><option><?php echo $fechaValor;?></option>
                                                    <?php
                                                    $fechaValor=$fechaValor+1;
                                                }?>
                                            </select>
                                        </th>
                                        <th id="cambio-volt">9.20mts</th>
                                        <th id="ancho_ext">2.60mts</th>
                                        <th id="cambio2-volt">1.25 mts</th>
                                        <th>
                                            <select name="ejes" id="ejes" class="ejes">
                                                <option value="2" class="no_ejes">2</option>
                                                <option value="3" class="no_ejes">3</option>
                                            </select></th>
                                    </tr>
                                    </tbody>
                                </table>

                        <?php
                            }elseif($_GET['id'] == 10) {
                                ?>
                                <table class="table table-striped table-hover table-bordered">
                                    <thead>
                                    <tr>
                                        <th>TIPO</th>
                                        <th>MODELO</th>
                                        <th>AÑO</th>
                                        <th>LARGO EXT</th>
                                        <th>ANCHO EXT</th>
                                        <th>ALTO T</th>
                                        <th>EJES</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <th>
                                            <select onchange="cajaSeca()" name="volteo" id="volteo" class="encortinado">
                                                <option value="PLATAFORMA DE 26' ENCORT TPM-AF-HQ" selected>PLATAFORMA DE 26' ENCORT TPM-AF-HQ</option>
                                                <option value="PLATAFORMA DE 30' ENCORT TPM-AF-HQ" >PLATAFORMA DE 30' ENCORT TPM-AF-HQ</option>
                                                <option value="PLATAFORMA DE 35' ENCORT TPM-AF-HQ" >PLATAFORMA DE 35' ENCORT TPM-AF-HQ</option>
                                                <option value="PLATAFORMA DE 38' ENCORT TPM-AF-HQ" >PLATAFORMA DE 38' ENCORT TPM-AF-HQ</option>
                                                <option value="PLATAFORMA DE 39' ENCORT TPM-AF-HQ" >PLATAFORMA DE 39' ENCORT TPM-AF-HQ</option>
                                                <option value="PLATAFORMA DE 40' ENCORT TPM-AF-HQ" >PLATAFORMA DE 40' ENCORT TPM-AF-HQ</option>
                                                <option value="PLATAFORMA DE 42' ENCORT TPM-AF-HQ" >PLATAFORMA DE 42' ENCORT TPM-AF-HQ</option>
                                                <option value="PLATAFORMA DE 45' ENCORT TPM-AF-HQ" >PLATAFORMA DE 45' ENCORT TPM-AF-HQ</option>
                                                <option value="PLATAFORMA DE 48' ENCORT TPM-AF-HQ" >PLATAFORMA DE 48' ENCORT TPM-AF-HQ</option>
                                            </select>
                                        </th>
                                        <th>
                                            <select name="modelo" id="modelo" class="modelo">
                                                <option value="TPM-AF-NACIONAL">TPM-AF-NACIONAL</option>
                                                <option value="TPM-AF-HENDRICKSON">TPM-AF-HENDRICKSON</option>
                                            </select>
                                        </th>
                                        <th>
                                            <?php $fechaValor=date("Y");?>
                                            <select id="dates">
                                                <?php
                                                for($i=2015;$i<=2025;$i++){
                                                    ?><option><?php echo $fechaValor;?></option>
                                                    <?php
                                                    $fechaValor=$fechaValor+1;
                                                }?>
                                            </select>
                                        </th>
                                        <th>
                                            <select name="largo" id="largo" class="largo-plataforma">
                                                <option value="7.92mts" class="largo">7.92mts</option>
                                                <option value="9.144mts" class="largo">9.144mts</option>
                                                <option value="10.67mts" class="largo">10.67mts</option>
                                                <option value="11.58mts" class="largo">11.58mts</option>
                                                <option value="11.88mts" class="largo">11.88mts</option>
                                                <option value="12.192mts" class="largo">12.192mts</option>
                                                <option value="12.80mts" class="largo">12.80mts</option>
                                                <option value="13.716mts" class="largo">13.716mts</option>
                                                <option value="14.63mts" class="largo">14.63mts</option>
                                            </select>
                                        </th>
                                        <th id="ancho_ext">2.60mts</th>
                                        <th>
                                            <select name="alto" id="alto">
                                                <option value="4.11mts">4.11mts</option>
                                            </select>
                                        </th>
                                        <th>
                                            <select name="ejes" id="ejes" class="ejes">
                                                <option value="2" class="no_ejes">2</option>
                                                <option value="3" class="no_ejes">3</option>
                                            </select></th>
                                    </tr>
                                    </tbody>
                                </table>
                            <?php
                            }
                        ?>
					</div>
				</div>
            
				<div class="row" id="parte-central">
					<div class="col-md-offset-1 col-md-10 principal">
                        <?php
                        //Datos Volteo
                        if($_GET['id']== 1) {

                            ?>
                            <section class="estructura">
                                <h4><strong> ESTRUCTURA</strong></h4>

                                <p id="poner">
                                    <strong>Vigas principales: </strong><small>Viga tipo "I" en 1/4 en acero de alta
                                    resisitencia ASTM A572.</small>
                                </p>

                                <p class="delete-cost">
                                    <strong>Costillas: </strong><small>De canal en acero de alta resistencia.</small>
                                </p>

                                <p>
                                    <strong>Cuerpo: </strong> <small class="body-inox30">Batea en acero de alta resistencia Cal. 3/16 y Cal. 10 ASTMA572.</small>
                                </p>

                                <p>
                                    <strong>Acoplador superior: </strong><small>Fijo soldado con placa de 3/8 con perno rey.</small>
                                    marca HOLLAND.
                                </p>

                                <p>
                                    <strong>Frente: </strong><small class="frente">Inclinado y reforzado en acero de alta resistencia.</small>
                                </p>

                                <p>
                                    <strong>Puerta trasera: </strong><small>A todo lo ancho y alto con refuerzos transversales con panel calibre 10.</small>
                                </p>

                                <p>
                                    <strong>Mecanismo doble en puerta trasera: </strong><small class="meca-puer">De operación neumática que fija ambos lados de la puerta y herraje.</small>
                                </p>

                                <p>
                                    <strong>Pistón hidráulico: </strong><small>Telescópico de 5 secciones importado.</small>
                                </p>
                            </section>
                            <section class="suspencion">
                                <h4><strong> SUSPENSIÓN</strong></h4>
                                <p>
                                    <strong>Suspensión: </strong><small>Neumática <input class="input-xlarge hendrix" id="hendrix" name="hendrix" value="Hendrickson" readonly/> HT-300 de 30,000lbs.</small>
                                </p>
                                <p>
                                    <strong>Ejes: </strong> <input type="text" class="ejes2"  name="ejes2" id="ejes2" readonly placeholder="(2)"/><small>En espesor de 3/4, capacidad de 30,000lbs.</small>
                                </p>
                                <p>
                                    <strong>Frenos: </strong><samall class="abs">Sistema ABS 4S/2M.</samll>
                                </p>
                                <p>
                                    <strong>Rines: </strong>
                                    <input type="text" class="rines"  name="rines" id="rines" readonly placeholder="(8)"/>
                                    <select onChange="rines()" id="rines-volteo">
                                        <option value="Rhims unimont 24.5.">Rhims unimont 24.5.</option>
                                        <option value="Rhims unimont 22.5.">Rhims unimont 22.5.</option>
                                    </select>
                                </p>
                                <p>
                                    <strong> <label for="llantas">Llantas:</label> </strong>
                                    <input type="text" class="llantas" name="no_llantas" id="no_llantas" readonly placeholder="(8)" />
                                    <!-- <small class="marck">11R24.5 marca</small> -->
                                    <select class="marck">
                                        <option value="11R24.5">11R24.5</option>
                                        <option value="11R22.5">11R22.5</option>
                                    </select>
                                    <select name="llantas" id="llantas">
                                        <option value="UNIROYAL">UNIROYAL</option>
                                        <option value="GENERAL">GENERAL</option>
                                        <option value="CONTINENTAL">CONTINENTAL</option>
                                        <option value="MICHELIN">MICHELIN</option>
                                        <option value="TRIANGLE">TRIANGLE</option>
                                        <option value="BFGODDRICH">BFGODDRICH</option>

                                    </select>
                                </p>
                                <p>
                                    <strong>Patines: </strong><small>Un juego mecánico de 2 velocidades marca HOLLAND.</small>
                                </p>
                                <p>
                                    <strong>Porta llantas: </strong><small>Tipo canastilla.</small>
                                </p>
                                <p>
                                    <strong>Retráctil: </strong><small>(1) Instalado en un eje.</small>
                                </p>
                            </section>

                            <section class="luces_pintura">
                                <h4><strong>LUCES Y PINTURA</strong></h4>

                                <p>
                                    <strong>Sistema eléctrico: </strong><small>Reglamentario con receptáculo de 7 hilos a 12 volts, plafones en LED.</small>
                                </p>
                                <p>
                                    <strong>Pintura: </strong><small>Con primarios epóxicos, esmalte automotivo al color requerido.</small>
                                </p>

                            </section>

                        <?php
                            //Datos Tolva
                        }elseif($_GET['id']== 2) {

                            ?>
                            <section class="estructura">
                                <h4><strong> ESTRUCTURA</strong></h4>

                                <p>
                                    <strong>Cuerpo:</strong><small> En lámina de acero al carbón cal.10.</small>
                                </p>

                                <p>
                                    <strong>Cerchas:</strong><small> Canal comercial en acero al carbón en cal.3/16".</small>
                                </p>

                                <p>
                                    <strong>Bastidor:</strong><small> En placa de alta resistencia cal.1/4".</small>
                                </p>

                                <p>
                                    <strong> Acoplador superior:</strong><small> Fijo, soldado y atornillado entre las vigas principales del bastidor   frontal, con placa de 3/8"  en acero de alta resistencia, con perno rey HOLLAND.</small>
                                </p>

                                <p>
                                    <strong>
                                        Domos:
                                    </strong>
                                    <select name="domos" id="domos">
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                    </select> <small> En aluminio de 20" de diámetro con herrajes de sujeción.<small>

                                </p>

                                <p>
                                    <strong>
                                        Manómetros:</strong><small> (2) De glicerina con 4" de caratula.</small>
                                </p>

                                <p>
                                    <strong>  Línea de alimentación:</strong><small> Tubería de 3" en acero negro ced.40.</small>
                                </p>

                                <p>
                                    <strong> Línea de descarga:</strong><small> Tubería de 4" en acero negro ced.40.</small>
                                </p>

                                <p>
                                    <strong> Válvulas de alivio:</strong><small> (3) En línea de alimentación al tanque.</small>
                                </p>

                                <p>
                                    <strong>
                                        Válvula check:</strong><small> Instalada en línea de alimentación. "T" Descarga de material: Fabricada en aluminio de bajo perfil con entrada de 6" y descarga para tubo de 4"; con válvulas de mariposa de 6" para control de la descarga.</small>
                                </p>

                            </section>
                            <section class="suspencion">
                                <h4><strong> SUSPENSIÓN</strong></h4>
                                <p>
                                    <strong>Suspensión:</strong><samll> Neumática</small><input class="input-xlarge hendrix" id="hendrix" name="hendrix" value="Hendrickson" readonly/><small>HT-300 de 30,000lbs.</small>
                                </p>
                                <p>
                                    <strong>Ejes:</strong> <input type="text" class="ejes2"  name="ejes2" id="ejes2" readonly placeholder="(2)"/><small>Track 77.5 con capacidad de carga de 30,000 libras.</small>
                                </p>
                                <p>
                                    <strong>Frenos:</strong><small class="abs"> Sistema ABS 2S/1M.</small>
                                </p>
                                <p>
                                    <strong>Rines: </strong>
                                    <input type="text" class="rines"  name="rines" id="rines" readonly placeholder="(8)"/>
                                    <select onChange="rines()" id="rines-volteo">
                                        <option value="Rhims unimont 24.5.">Rhims unimont 24.5.</option>
                                        <option value="Rhims unimont 22.5.">Rhims unimont 22.5.</option>
                                    </select>
                                </p>
                                <p>
                                    <strong> <label for="llantas">Llantas:</label> </strong>
                                    <input type="text" class="llantas" name="no_llantas" id="no_llantas" readonly placeholder="(8)" />
                                    <!-- <small class="marck">11R24.5 marca</small> -->
                                   <select class="marck">
                                        <option value="11R24.5">11R24.5</option>
                                        <option value="11R22.5">11R22.5</option>
                                    </select>
                                    <select name="llantas" id="llantas">
                                        <option value="UNIROYAL">UNIROYAL</option>
                                        <option value="GENERAL">GENERAL</option>
                                        <option value="CONTINENTAL">CONTINENTAL</option>
                                        <option value="MICHELIN">MICHELIN</option>
                                        <option value="TRIANGLE">TRIANGLE</option>
                                        <option value="BFGODDRICH">BFGODDRICH</option>

                                    </select>
                                </p>
                                <p>
                                    <strong>Patines:</strong><small> Un juego; mecánico de 2 velocidades marca HOLLAND.</small>
                                </p>
                                <p>
                                    <strong>Porta llantas:</strong><small> Tipo canastilla.</small>
                                </p>
                                <p>
                                    <strong>Fenders:</strong><small> De aluminio en la parte trasera y delantera del tanque.</small>
                                </p>

                            </section>
                            <section class="luces_pintura">
                                <h4><strong>LUCES Y PINTURA</strong></h4>

                                <p>
                                    <strong>Sistema eléctrico:</strong><small> Reglamentario SCT, luces LED, enchufes y arnés importado.</small>
                                </p>
                                <p>
                                    <strong>Pintura:</strong><small> En chasis con primario epóxico, esmalte automotivo.</small>
                                </p>
                            </section>

                        <?php
                            //Datos Dolly
                        }elseif($_GET['id'] == 3) {
                            ?>
                            <section class="estructura">
                                <h4><strong> ESTRUCTURA</strong></h4>
                                <p><strong>Estructura: </strong><small>Fabricada con placa calibre de ½", 3/8" Y ¼" en calidad ASTM A572</small></p>
                            </section>

                            <section class="suspencion">
                                <!-- <h4><strong> SUSPENCIÓN</strong></h4> -->
                                <h4><strong>SUSPENSIÓN</strong></h4>
                                <p>
                                    <strong>Suspensión: </strong><small>Neumática <input class="input-xlarge hendrix" id="hendrix" name="hendrix" value="Hendrickson" readonly/> HT-300 de 30,000lbs.</small>
                                </p>
                                <p>
                                    <strong>Ejes: </strong><small>(2) Tubular redondo de espesor en 3/4 30,000 LBS</small>
                                </p>
                                <p>
                                    <strong>Sistema de frenos: </strong><small>ABS 2S/1M</small>
                                </p>
                                <p>
                                    <strong>Lanza: </strong><small>Marca Holland o Premier, incluyendo tiro con bisagras.</small>
                                </p>
                                <p>
                                    <strong>Quinta rueda: </strong><small>Marca HOLLAND u JOST</small>
                                </p>
                                <h4><strong>Cadenas y argollas de seguridad</strong></h4>
                                <p>
                                    <strong>Rines: </strong>
                                    <input type="text" class="rines"  name="rines" id="rines" readonly placeholder="(8)"/>
                                    <select onChange="rines()" id="rines-volteo">
                                        <option value="Rhims unimont 24.5.">Rhims unimont 24.5.</option>
                                        <option value="Rhims unimont 22.5.">Rhims unimont 22.5.</option>
                                    </select>
                                </p>
                                <p>
                                    <strong> <label for="llantas">Llantas:</label> </strong>
                                    <input type="text" class="llantas" name="no_llantas" id="no_llantas" readonly placeholder="(8)" />
                                    <!-- <small class="marck">11R24.5 marca</small> -->
                                    <select class="marck">
                                        <option value="11R24.5">11R24.5</option>
                                        <option value="11R22.5">11R22.5</option>
                                    </select>
                                    <select name="llantas" id="llantas">
                                        <option value="UNIROYAL">UNIROYAL</option>
                                        <option value="GENERAL">GENERAL</option>
                                        <option value="CONTINENTAL">CONTINENTAL</option>
                                        <option value="MICHELIN">MICHELIN</option>
                                        <option value="TRIANGLE">TRIANGLE</option>
                                        <option value="BFGODDRICH">BFGODDRICH</option>

                                    </select>
                                </p>
                            </section>

                            <section class="luces_pintura">
                                <h4><strong>LUCES Y PINTURA</strong></h4>
                               <p><strong> Sistema eléctrico: </strong><small>Reglamentario con receptáculo de 7 hilos a 12 volts, luces reglamentarias según SCT.</small></p>
                                <p><strong>Pintura: </strong><small>Con primarios epóxicos, esmalte automotivo al color requerido.</small></p>

                            </section>
                        <?php
                            //Datos caja seca
                        }elseif($_GET['id']== 4) {

                            ?>
                            <section class="estructura">
                                <h4><strong> ESTRUCTURA</strong></h4>

                                <p>
                                    <strong>
                                        Acoplador superior:</strong><small> Con perno fijo localizado a 36", bastidor a todo lo ancho de la unidad en cal. 5/16".</small>
                                </p>

                                <p>
                                    <strong> Puentes:</strong><small> Viga "I" a cada 9" en el sub-bastidor y a cada 12" en parte intermedia.</small>
                                </p>
                               <p>
                                   <strong>
                                       Piso:</strong><small> Madera de pino de 1 1/2 de espesor, con recubrimiento de piso laminado antiderrapante, zoclo en galvanizado con solera de ¼ de refuerzo.</small>
                               </p>
                                <p>
                                    <strong>
                                        Postes logísticos:</strong><small> Canal de acero galvanizado localizados a 12".</small>
                                </p>
                                <p>
                                    <strong>
                                        Postes frontales:
                                    </strong><small> Canal de acero galvanizado.</small>
                                </p>

                                <p>
                                    <strong>
                                        Borda superior e inferior:
                                    </strong><small> De aluminio extruido sujetada con remache de aluminio.</small>
                                </p>

                                <p>
                                    <strong>
                                        Paneles:
                                    </strong><small> Aluminio blanco wash y zoclo en lámina galvanizada de 12".</small>
                                </p>
                                <p>
                                    <strong>
                                        Puerta trasera:
                                    </strong><small> Tipo cortina a todo lo ancho y alto, con sus respectivos cerrojos y gancho.</small>
                                </p>

                                <p>
                                    <strong>
                                        Techo:
                                    </strong><small> Aluminio de una sola pieza en cal. 19.</small>
                                </p>

                                <p>
                                    <strong>
                                        Forro interior:
                                    </strong><small> En triplay de 6 mm en los costados y parte frontal.</small>
                                </p>

                                <p>
                                    <strong>
                                        Portallanta:
                                    </strong><small> Tipo canastilla en lado derecho.</small>
                                </p>

                            </section>
                            <section class="suspencion">
                                <h4><strong> SUSPENSIÓN</strong></h4>
                                <p>
                                    <strong>Suspensión:</strong> Neumática <input class="input-xlarge hendrix" id="hendrix" name="hendrix" value="Hendrickson" readonly/> HT-300 de 30,000lbs.
                                    <input class="deslizable input-xlarge" id="deslizable" name="deslizable" type="text" value="" style="display: none" readonly/>
                                </p>
                                <p>
                                    <strong>Ejes:</strong> <input type="text" class="ejes2"  name="ejes2" id="ejes2" readonly placeholder="(2)"/> En espesor de 3/4, capacidad de 30,000lbs.
                                </p>
                                <p>
                                    <strong>Frenos:</strong><small class="abs"> Sistema ABS 4S/2M marca WABCO NOM-SCT035.</small>
                                </p>
                                <p>
                                    <strong>Rines: </strong>
                                    <input type="text" class="rines"  name="rines" id="rines" readonly placeholder="(8)"/>
                                    <select onChange="rines()" id="rines-volteo">
                                        <option value="Rhims unimont 24.5.">Rhims unimont 24.5.</option>
                                        <option value="Rhims unimont 22.5.">Rhims unimont 22.5.</option>
                                    </select>
                                </p>
                                <p>
                                    <strong> <label for="llantas">Llantas:</label> </strong>
                                    <input type="text" class="llantas" name="no_llantas" id="no_llantas" readonly placeholder="(8)" />
                                    <!-- <small class="marck">11R24.5 marca</small> -->
                                    <select class="marck">
                                        <option value="11R24.5">11R24.5</option>
                                        <option value="11R22.5">11R22.5</option>
                                    </select>
                                    <select name="llantas" id="llantas">
                                        <option value="UNIROYAL">UNIROYAL</option>
                                        <option value="GENERAL">GENERAL</option>
                                        <option value="CONTINENTAL">CONTINENTAL</option>
                                        <option value="MICHELIN">MICHELIN</option>
                                        <option value="TRIANGLE">TRIANGLE</option>
                                        <option value="BFGODDRICH">BFGODDRICH</option>

                                    </select>
                                </p>
                                <p>
                                    <strong>Patines:</strong><small> Un juego mecánico de 2 velocidades marca HOLLAND.</small>
                                </p>

                            </section>
                            <section class="luces_pintura">
                                <h4><strong>LUCES Y PINTURA</strong></h4>

                                <p>
                                    <strong>
                                        Sistema eléctrico:</strong><small> Reglamentario SCT, luces LED, enchufes y arnés importado marca GROTE.</small>
                                </p>
                                <p>
                                    <strong>
                                        Pintura:</strong><small> Primario epóxico con esmalte automotivo, al color requerido.</small>
                                </p>

                            </section>

                        <?php
                            //plataforma
                        } elseif($_GET['id'] == 5) {
                        ?>
                            <section class="estructura">
                                <h4><strong> ESTRUCTURA</strong></h4>
                                <p>
                                    <strong>
                                        Vigas principales: </strong><small>Viga tipo "I" formada con acero de alta resistencia ASTM-A572.</small>
                                </p>
                                <p>
                                    <strong>
                                        Puentes travesaños: </strong><small>Tipo "I" estructural de 4" de 80,000 lbs.</small>
                                </p>
                                <p>
                                    <strong>
                                        Acoplador superior: 
                                    </strong><small>Fijo soldado con placa de 3/8" con perno rey marca HOLLAND.</small>
                                </p>
                                <p>
                                    <strong>
                                        Defensa: 
                                    </strong><small>Tipo estribo soldada a las vigas principales, con canal estructural de 6”.</small>
                                </p>
                                <p>
                                    <strong>
                                        Piso: </strong><small>De madera de pino de 1 1/2. de espesor.</small>
                                </p>
                                <p>
                                    <strong>Concha: </strong><small>Fabricada en ptr y lámina a una altura de 1.20</small>
                                </p>

                            </section>
                            <section class="suspencion">
                                <h4><strong> SUSPENSIÓN</strong></h4>
                                <p>
                                    <strong>Suspensión: </strong><small>Neumática <input class="input-xlarge hendrix" id="hendrix" name="hendrix" value="Hendrickson" readonly/> HT-300 de 30,000lbs.</small>
                                </p>
                                <p>
                                    <strong>Ejes: </strong> <input type="text" class="ejes2"  name="ejes2" id="ejes2" readonly placeholder="(2)"/><small>En espesor de 3/4, capacidad de 30,000lbs.</small>
                                </p>
                                <p>
                                    <strong>Frenos: </strong><small id="plata-fre">Sistema ABS 4S/2M.</small>
                                </p>
                                <p>
                                    <strong>Rines: </strong>
                                    <input type="text" class="rines"  name="rines" id="rines" readonly placeholder="(8)"/>
                                    <select onChange="rines()" id="rines-volteo">
                                        <option value="Rhims unimont 24.5.">Rhims unimont 24.5.</option>
                                        <option value="Rhims unimont 22.5.">Rhims unimont 22.5.</option>
                                    </select>
                                </p>
                                <p>
                                    <strong> <label for="llantas">Llantas:</label> </strong>
                                    <input type="text" class="llantas" name="no_llantas" id="no_llantas" readonly placeholder="(8)" />
                                    <!-- <small class="marck">11R24.5 marca</small> -->
                                    <select class="marck">
                                        <option value="11R24.5">11R24.5</option>
                                        <option value="11R22.5">11R22.5</option>
                                    </select>
                                    <select name="llantas" id="llantas">
                                        <option value="UNIROYAL">UNIROYAL</option>
                                        <option value="GENERAL">GENERAL</option>
                                        <option value="CONTINENTAL">CONTINENTAL</option>
                                        <option value="MICHELIN">MICHELIN</option>
                                        <option value="TRIANGLE">TRIANGLE</option>
                                        <option value="BFGODDRICH">BFGODDRICH</option>

                                    </select>
                                </p>
                                <p>
                                    <strong>Patines: </strong><small>Un juego; mecánicos de 2 velocidades marca HOLLAND.</small>
                                </p>
                                <p>
                                    <strong>Porta llantas: </strong><small>Tipo canastilla.</small>
                                </p>
                                <!-- <p>
                                    <strong>Gancho de arrastre: </strong><small>Marca HOLLAND.</small>
                                </p> -->

                            </section>
                            <section class="luces_pintura">
                                <h4><strong>LUCES Y PINTURA</strong></h4>

                                <p>
                                    <strong>
                                        Sistema eléctrico: 
                                    </strong><small>Reglamentario SCT, luces LED, enchufes y arnés importado marca GROTE.</small>
                                </p>
                                <p>
                                    <strong>
                                        Pintura: 
                                    </strong><small>Primario epóxico con esmalte automotivo, al color requerido.</small>
                                </p>

                            </section>
                       <?php
                        } elseif($_GET['id']== 6) {

                           ?>
                            <section class="estructura">
                                <h4><strong> ESTRUCTURA</strong></h4>
                                <p>
                                    <strong>
                                        Vigas principales: </strong><small> Viga tipo "I" formada con acero de alta resistencia ASTM-A572.</small>
                                </p>
                                <p>
                                    <strong>
                                        Puentes travesaños: </strong><small> Tipo "I" estructural de 4" de 80,000 lbs.</small>
                                </p>
                                <p>
                                    <strong>
                                        Acoplador superior: 
                                    </strong><small> Fijo soldado con placa de 3/8" con perno rey marca HOLLAND.</small>
                                </p>
                                <p>
                                    <strong>
                                        Defensa: 
                                    </strong><small> Tipo estribo soldada a las vigas principales, con canal estructural de 6".</small>
                                </p>
                                <p>
                                    <strong>
                                        Candados: 
                                    </strong><small> 4 fijos frontales y 10 twist lock intermedios de fundición.</small>
                                </p>

                            </section>
                            <section class="suspencion">
                                <h4><strong> SUSPENSIÓN</strong></h4>
                                <p>
                                    <strong>Suspensión: </strong><small>Neumática <input class="input-xlarge hendrix" id="hendrix" name="hendrix" value="Hendrickson" readonly/> HT-300 de 30,000lbs.</small>
                                </p>
                                <p>
                                    <strong>Ejes: </strong> <input type="text" class="ejes2"  name="ejes2" id="ejes2" readonly placeholder="(2)"/><small>En espesor de 3/4, capacidad de 30,000lbs.</small>
                                </p>
                                <p>
                                    <strong>Frenos: </strong><small id="plata-fre">Sistema ABS 2S/1M.</small>
                                </p>
                                <p>
                                    <strong>Rines:</strong>
                                    <input type="text" class="rines"  name="rines" id="rines" readonly placeholder="(8)"/>
                                    <small>Rhims unimont 22.5.</small>
                                </p>
                                <p>
                                    <strong> <label for="llantas">Llantas:</label> </strong>
                                    <input type="text" class="llantas" name="no_llantas" id="no_llantas" readonly placeholder="(8)" />
                                    <small>11R22.5 marca</small>
                                    <select name="llantas" id="llantas">
                                        <option value="UNIROYAL">UNIROYAL</option>
                                        <option value="GENERAL">GENERAL</option>
                                        <option value="CONTINENTAL">CONTINENTAL</option>
                                        <option value="MICHELIN">MICHELIN</option>
                                        <option value="TRIANGLE">TRIANGLE</option>
                                        <option value="BFGODDRICH">BFGODDRICH</option>

                                    </select>
                                    .
                                </p>
                                <p>
                                    <strong>Patines: </strong><small>Un juego; mecánicos de 2 velocidades marca HOLLAND.</small>
                                </p>
                                <p>
                                    <strong>Porta llantas: </strong><small>Tipo canastilla.</small>
                                </p>
                                <p>
                                    <strong>Gancho: </strong><small> De arrastre marca HOLLAND o PREMIER.</small>
                                </p>
                                <!-- <p>
                                    <strong>Gancho de arrastre: </strong><small>Marca HOLLAND.</small>
                                </p> -->

                            </section>
                            <section class="luces_pintura">
                                <h4><strong>LUCES Y PINTURA</strong></h4>

                                <p>
                                    <strong>
                                        Sistema eléctrico: 
                                    </strong><small>Reglamentario SCT, luces LED, enchufes y arnés importado marca GROTE.</small>
                                </p>
                                <p>
                                    <strong>
                                        Pintura: 
                                    </strong><small>Primario epóxico con esmalte automotivo, al color requerido.</small>
                                </p>

                            </section>
                       <?php
                        }elseif($_GET['id'] == 7) {
                            ?>
                            <section class="estructura">
                                <h4><strong> ESTRUCTURA</strong></h4>
                                <p>
                                    <strong>
                                        Vigas principales: </strong><small> Viga tipo "I" formada con acero de alta resistencia ASTM-A572.</small>
                                </p>
                                <p>
                                    <strong>
                                        Puentes travesaños: </strong><small> Tipo "C" estructural en ASTM-A572 para puentes de suspensión.</small>
                                </p>
                                <p>
                                    <strong>
                                        Acoplador superior: 
                                    </strong><small> Fijo soldado con placa de 3/8" con perno rey marca HOLLAND.</small>
                                </p>
                                <p>
                                    <strong>
                                        Defensa: 
                                    </strong><small> Tipo estribo soldada a las vigas principales, con canal estructural de 6".</small>
                                </p>
                                <p>
                                    <strong>
                                        Rodamientos: 
                                    </strong><small> Fabricados y maquinados en aceros especiales para rodamiento del Chasis.</small>
                                </p>
                                <p>
                                    <strong>
                                        Candados: 
                                    </strong><small> 4 fijos frontales y 10 twist lock intermedios de fundición.</small>
                                </p>

                            </section>
                            <section class="suspencion">
                                <h4><strong> SUSPENSIÓN</strong></h4>
                                <p>
                                    <strong>Suspensión: </strong><small>Neumática <input class="input-xlarge hendrix" id="hendrix" name="hendrix" value="Hendrickson" readonly/> HT-300 de 30,000lbs.</small>
                                </p>
                                <p>
                                    <strong>Ejes: </strong> <input type="text" class="ejes2"  name="ejes2" id="ejes2" readonly placeholder="(2)"/><small>En espesor de 3/4, capacidad de 30,000lbs.</small>
                                </p>
                                <p>
                                    <strong>Frenos: </strong><small id="plata-fre">Sistema ABS 2S/1M.</small>
                                </p>
                                <p>
                                    <strong>Rines:</strong>
                                    <input type="text" class="rines"  name="rines" id="rines" readonly placeholder="(8)"/>
                                    <small>Rhims unimont 22.5.</small>
                                </p>
                                <p>
                                    <strong> <label for="llantas">Llantas:</label> </strong>
                                    <input type="text" class="llantas" name="no_llantas" id="no_llantas" readonly placeholder="(8)" />
                                    <small>11R22.5 marca</small>
                                    <select name="llantas" id="llantas">
                                        <option value="UNIROYAL">UNIROYAL</option>
                                        <option value="GENERAL">GENERAL</option>
                                        <option value="CONTINENTAL">CONTINENTAL</option>
                                        <option value="MICHELIN">MICHELIN</option>
                                        <option value="TRIANGLE">TRIANGLE</option>
                                        <option value="BFGODDRICH">BFGODDRICH</option>

                                    </select>
                                    .
                                </p>
                                <p>
                                    <strong>Patines: </strong><small>Un juego; mecánicos de 2 velocidades marca HOLLAND.</small>
                                </p>
                                <p>
                                    <strong>Porta llantas: </strong><small>Tipo canastilla.</small>
                                </p>
                                <p>
                                    <strong>Gancho: </strong><small> De arrastre marca HOLLAND o PREMIER.</small>
                                </p>
                                <!-- <p>
                                    <strong>Gancho de arrastre: </strong><small>Marca HOLLAND.</small>
                                </p> -->

                            </section>
                            <section class="luces_pintura">
                                <h4><strong>LUCES Y PINTURA</strong></h4>

                                <p>
                                    <strong>
                                        Sistema eléctrico: 
                                    </strong><small>Reglamentario SCT, luces LED, enchufes y arnés importado marca GROTE.</small>
                                </p>
                                <p>
                                    <strong>
                                        Pintura: 
                                    </strong><small>Primario epóxico con esmalte automotivo, al color requerido.</small>
                                </p>

                            </section>
                       <?php
                        } elseif($_GET['id'] == 8) {
                        ?>
                            <section class="estructura">
                                <h4><strong> ESTRUCTURA</strong></h4>
                                <p>
                                    <strong>
                                        Vigas principales: </strong><small>Viga tipo "I" formada con acero de alta resistencia ASTM-A572.</small>
                                </p>
                                <p>
                                    <strong>
                                        Puentes travesaños: </strong><small>Tipo "I" estructural de 4" de 80,000 lbs.</small>
                                </p>
                                <p>
                                    <strong>
                                        Acoplador superior: 
                                    </strong><small>Fijo soldado con placa de 3/8" con perno rey marca HOLLAND.</small>
                                </p>
                                <p>
                                    <strong>
                                        Defensa: 
                                    </strong><small>Tipo estribo soldada a las vigas principales, con canal estructural de 6”.</small>
                                </p>
                                <p>
                                    <strong>
                                        Piso: </strong><small>De madera de pino de 1 1/2. de espesor.</small>
                                </p>
                                <p>
                                    <strong>Candados: </strong><small> (12) Twist Lock de fundición.</small>
                                </p>

                            </section>
                            <section class="suspencion">
                                <h4><strong> SUSPENSIÓN</strong></h4>
                                <p>
                                    <strong>Suspensión: </strong><small>Neumática <input class="input-xlarge hendrix" id="hendrix" name="hendrix" value="Hendrickson" readonly/> HT-300 de 30,000lbs.</small>
                                </p>
                                <p>
                                    <strong>Ejes: </strong> <input type="text" class="ejes2"  name="ejes2" id="ejes2" readonly placeholder="(2)"/><small>En espesor de 3/4, capacidad de 30,000lbs.</small>
                                </p>
                                <p>
                                    <strong>Frenos: </strong><small id="plata-fre">Sistema ABS 2S/1M.</small>
                                </p>
                                <p>
                                    <strong>Rines:</strong>
                                    <input type="text" class="rines"  name="rines" id="rines" readonly placeholder="(8)"/>
                                    <small>Rhims unimont 22.5.</small>
                                </p>
                                <p>
                                    <strong> <label for="llantas">Llantas:</label> </strong>
                                    <input type="text" class="llantas" name="no_llantas" id="no_llantas" readonly placeholder="(8)" />
                                    <small>11R22.5 marca</small>
                                    <select name="llantas" id="llantas">
                                        <option value="UNIROYAL">UNIROYAL</option>
                                        <option value="GENERAL">GENERAL</option>
                                        <option value="CONTINENTAL">CONTINENTAL</option>
                                        <option value="MICHELIN">MICHELIN</option>
                                        <option value="TRIANGLE">TRIANGLE</option>
                                        <option value="BFGODDRICH">BFGODDRICH</option>

                                    </select>
                                    .
                                </p>
                                <p>
                                    <strong>Patines: </strong><small>Un juego; mecánicos de 2 velocidades marca HOLLAND.</small>
                                </p>
                                <p>
                                    <strong>Porta llantas: </strong><small>Tipo canastilla.</small>
                                </p>
                                <p>
                                    <strong>Gancho: </strong><small> De arrastre marca HOLLAND o PREMIER.</small>
                                </p>
                                <!-- <p>
                                    <strong>Gancho de arrastre: </strong><small>Marca HOLLAND.</small>
                                </p> -->

                            </section>
                            <section class="luces_pintura">
                                <h4><strong>LUCES Y PINTURA</strong></h4>

                                <p>
                                    <strong>
                                        Sistema eléctrico: 
                                    </strong><small>Reglamentario SCT, luces LED, enchufes y arnés importado marca GROTE.</small>
                                </p>
                                <p>
                                    <strong>
                                        Pintura: 
                                    </strong><small>Primario epóxico con esmalte automotivo, al color requerido.</small>
                                </p>

                            </section>
                       <?php
                        }else if($_GET['id']== 9) {

                            ?>
                            <section class="estructura">
                                <h4><strong> ESTRUCTURA</strong></h4>

                                <p id="poner">
                                    <strong>Vigas principales: </strong><small>Viga tipo "I" en 1/4 en acero de alta
                                    resisitencia ASTM A572.</small>
                                </p>

                                <p>
                                    <strong>Puentes travesaños: </strong><small> Tipo "I" estructural de 4" pulgadas en 80,000 lbs.</small>
                                </p>

                                <p>
                                    <strong>Cuerpo: </strong> <small class="body-inox30">Batea en acero de alta resistencia Cal. 1/4 y Cal. 10 ASTMA572.</small>
                                </p>

                                <p>
                                    <strong>Acoplador superior: </strong><small>Fijo soldado con placa de 3/8 con perno rey.</small>
                                    marca HOLLAND.
                                </p>

                                <p>
                                    <strong>Frente: </strong><small class="frente">Inclinado y reforzado en acero de alta resistencia.</small>
                                </p>

                                <p>
                                    <strong>Puerta trasera: </strong><small>A todo lo ancho y alto con refuerzos transversales con panel calibre 10.</small>
                                </p>

                                <p>
                                    <strong>Mecanismo doble en puerta trasera: </strong><small class="meca-puer">De operación neumática que fija ambos lados de la puerta y herraje.</small>
                                </p>

                                <p>
                                    <strong>Pistón hidráulico: </strong><small>Telescópico de 5 secciones importado.</small>
                                </p>
                                <p>
                                    <strong>Redilas: </strong><small> Ensambladas en acero de alta resistencia a una altura de 1.25mts con sus respectivos cerrojos.</small>
                                </p>
                            </section>
                            <section class="suspencion">
                                <h4><strong> SUSPENSIÓN</strong></h4>
                                <p>
                                    <strong>Suspensión: </strong><small>Neumática <input class="input-xlarge hendrix" id="hendrix" name="hendrix" value="Hendrickson" readonly/> HT-300 de 30,000lbs.</small>
                                </p>
                                <p>
                                    <strong>Ejes: </strong> <input type="text" class="ejes2"  name="ejes2" id="ejes2" readonly placeholder="(2)"/><small>En espesor de 3/4, capacidad de 30,000lbs.</small>
                                </p>
                                <p>
                                    <strong>Frenos: </strong><samall class="abs">Sistema ABS 4S/2M.</samll>
                                </p>
                                <p>
                                    <strong>Rines: </strong>
                                    <input type="text" class="rines"  name="rines" id="rines" readonly placeholder="(8)"/>
                                    <select onChange="rines()" id="rines-volteo">
                                        <option value="Rhims unimont 24.5.">Rhims unimont 24.5.</option>
                                        <option value="Rhims unimont 22.5.">Rhims unimont 22.5.</option>
                                    </select>
                                </p>
                                <p>
                                    <strong> <label for="llantas">Llantas:</label> </strong>
                                    <input type="text" class="llantas" name="no_llantas" id="no_llantas" readonly placeholder="(8)" />
                                    <!-- <small class="marck">11R24.5 marca</small> -->
                                    <select class="marck">
                                        <option value="11R24.5">11R24.5</option>
                                        <option value="11R22.5">11R22.5</option>
                                    </select>
                                    <select name="llantas" id="llantas">
                                        <option value="UNIROYAL">UNIROYAL</option>
                                        <option value="GENERAL">GENERAL</option>
                                        <option value="CONTINENTAL">CONTINENTAL</option>
                                        <option value="MICHELIN">MICHELIN</option>
                                        <option value="TRIANGLE">TRIANGLE</option>
                                        <option value="BFGODDRICH">BFGODDRICH</option>

                                    </select>
                                </p>
                                <p>
                                    <strong>Patines: </strong><small>Un juego mecánico de 2 velocidades marca HOLLAND.</small>
                                </p>
                                <p>
                                    <strong>Porta llantas: </strong><small>Tipo canastilla.</small>
                                </p>
                                <p>
                                    <strong>Retráctil: </strong><small>(1) Instalado en un eje.</small>
                                </p>
                            </section>

                            <section class="luces_pintura">
                                <h4><strong>LUCES Y PINTURA</strong></h4>

                                <p>
                                    <strong>Sistema eléctrico: </strong><small>Reglamentario con receptáculo de 7 hilos a 12 volts, plafones en LED.</small>
                                </p>
                                <p>
                                    <strong>Pintura: </strong><small>Con primarios epóxicos, esmalte automotivo al color requerido.</small>
                                </p>

                            </section>

                        <?php
                            //Datos Tolva
                        }else if($_GET['id']== 10) {

                            ?>
                            <section class="estructura">
                                <h4><strong> ESTRUCTURA</strong></h4>

                                <p id="poner">
                                    <strong>Vigas principales: </strong><small>Viga tipo "I" en 1/4 en acero de alta
                                    resisitencia ASTM A572.</small>
                                </p>
                                <p>
                                    <strong>Acoplador superior: </strong><small>Fijo soldado con placa de 3/8 con perno rey.</small>
                                    marca HOLLAND.
                                </p>

                                <p>
                                    <strong>Defensa: </strong><small>Tipo estribo soldada a las vigas principales.</small>
                                </p>

                                <p>
                                    <strong>Piso: </strong><small>De madera de pino de 1 1/2. De espesor, con piso antiderrapante cal. 12.</small>
                                </p>

                                <p>
                                    <strong>Pantalla frontal: </strong><small>Con estructura en acero al carbón y exterior aluminio blanco wash.</small>
                                </p>

                                <p>
                                    <strong>Estructura del toldo: </strong><small>De aluminio en una sola pieza Cal. 19, con cerchas de aluminio extruido 6061-T6, con sus respectivos caballetes laterales para soporte de estructura.</small>
                                </p>
                                <p>
                                    <strong>Cortinas: </strong><small>Al color y logo requerido por el cliente importada 900gr/m2 con su respectivo bota aguas de hule pelmet.</small>
                                </p>
                                <p>
                                    <strong>Puertas traseras: </strong><small>Tipo sandwich abatibles incluyendo sus respectivas bisagras y cerrojos para candados.</small>
                                </p>
                            </section>
                            <section class="suspencion">
                                <h4><strong> SUSPENSIÓN</strong></h4>
                                <p>
                                    <strong>Suspensión: </strong><small>Neumática <input class="input-xlarge hendrix" id="hendrix" name="hendrix" value="Hendrickson" readonly/> HT-300 de 30,000lbs.</small>
                                </p>
                                <p>
                                    <strong>Ejes: </strong> <input type="text" class="ejes2"  name="ejes2" id="ejes2" readonly placeholder="(2)"/><small>En espesor de 3/4, capacidad de 30,000lbs.</small>
                                </p>
                                <p>
                                    <strong>Frenos: </strong><samall class="abs">Sistema ABS 4S/2M.</samll>
                                </p>
                                <p>
                                    <strong>Rines: </strong>
                                    <input type="text" class="rines"  name="rines" id="rines" readonly placeholder="(8)"/>
                                    <select onChange="rines()" id="rines-volteo">
                                        <option value="Rhims unimont 24.5.">Rhims unimont 24.5.</option>
                                        <option value="Rhims unimont 22.5.">Rhims unimont 22.5.</option>
                                    </select>
                                </p>
                                <p>
                                    <strong> <label for="llantas">Llantas:</label> </strong>
                                    <input type="text" class="llantas" name="no_llantas" id="no_llantas" readonly placeholder="(8)" />
                                    <!-- <small class="marck">11R24.5 marca</small> -->
                                    <select class="marck">
                                        <option value="11R24.5">11R24.5</option>
                                        <option value="11R22.5">11R22.5</option>
                                    </select>
                                    <select name="llantas" id="llantas">
                                        <option value="UNIROYAL">UNIROYAL</option>
                                        <option value="GENERAL">GENERAL</option>
                                        <option value="CONTINENTAL">CONTINENTAL</option>
                                        <option value="MICHELIN">MICHELIN</option>
                                        <option value="TRIANGLE">TRIANGLE</option>
                                        <option value="BFGODDRICH">BFGODDRICH</option>

                                    </select>
                                </p>
                                <p>
                                    <strong>Patines: </strong><small>Un juego mecánico de 2 velocidades marca HOLLAND.</small>
                                </p>
                                <p>
                                    <strong>Porta llantas: </strong><small>Tipo canastilla.</small>
                                </p>
                            </section>

                            <section class="luces_pintura">
                                <h4><strong>LUCES Y PINTURA</strong></h4>

                                <p>
                                    <strong>Sistema eléctrico: </strong><small>Reglamentario SCT, luces en LEDS, enchunfes y arnés importaddo.</small>
                                </p>
                                <p>
                                    <strong>Pintura: </strong><small>Con primarios epóxicos, esmalte automotivo al color requerido.</small>
                                </p>

                            </section>

                        <?php
                            //Datos Tolva
                        }
                        ?>
					</div>
				</div>


				<div class="row">
					<div class="col-md-offset-1 col-md-10">
                        <table id="items" class="table table-responsive" border="0">

            			    <tbody>
                            <tr>
                                <th></th>
                                <th>Precio</th>
                                <th>Iva</th>
                                <th>Total</th>
                            </tr>
                                <tr class="item-row">
                                    <td class="item-name">
                                        <div>
                                            <strong>Precio vehiculo</strong>
                                        </div>
                                    </td>

                                    <td>

                                    <input type="text" id="cantidad_precio" class="cost" id="precio_cotiza" name="precio_cotiza" placeholder="$0.00"/>
                                    </td>

                                    <td>
                                        <input type="text" class="price iv" id="precio_veh" name="precio_veh" disabled readonly placeholder="$0.00"/>
                                    </td>

                                    <td>
                                        <input type="text" class="iva" id="total_v" name="total_v" disabled readonly placeholder="$0.00"/>
                                    </td>
                                </tr>
                                <tr>
                                <td colspan="4">
                                    <strong style="font-size:25px;">Opcionales</strong>
                                </td>
                            </tr>
                            <?php
                            //opcionales
                                if ($_GET['id'] == 1) {
                                   ?>
                                   <tr class="item-row">
                            <td class="item-name">
                                <div>
                                    <strong>Patin Extra</strong>
                                </div>
                            </td>

                            <td>
                                <input type="text" class="cost opc" id="patin_ext" name="patin_ext" placeholder="$0.00"/>
                            </td>

                            <td>
                                <input type="text" class="price iv" id="precio_u1" name="precio_u1" disabled readonly placeholder="$0.00"/>
                            </td>
                            <td>
                                <input type="text" class="iva" id="total" name="total" disabled readonly placeholder="$0.00"/>
                            </td>
                        </tr>

                        <tr class="item-row">
                            <td class="item-name">
                                <div >
                                    <strong>Retractil 1 eje</strong>
                                </div>
                            </td>

                            <td>
                                <input type="text" class="cost opc" id="retractil_1" name="retractil_1" placeholder="$0.00"/>
                            </td>

                            <td>
                                <input type="text" class="price iv" id="precio_u2" name="precio_u2" disabled readonly placeholder="$0.00"/>
                            </td>

                            <td>
                                <input type="text" class="iva" id="total1" name="total1" disabled readonly placeholder="$0.00"/>
                            </td>
                        </tr>
                        <tr class="item-row">
                            <td class="item-name">
                                <div >
                                    <strong>Retractil 2 ejes</strong>
                                </div>
                            </td>

                            <td>
                                <input type="text" class="cost opc" id="retractil_2" name="retractil_2" placeholder="$0.00"/>
                            </td>

                            <td>
                                <input type="text" class="price iv" id="precio_u3" name="precio_u3" disabled readonly placeholder="$0.00"/>
                            </td>

                            <td>
                                <input type="text" class="iva" id="total2" name="total2" disabled readonly placeholder="$0.00"/>
                            </td>
                        </tr>

                        <tr class="item-row">
                            <td class="item-name">
                                <div >
                                    <strong>Caja de herramientas</strong>
                                </div>
                            </td>

                            <td>
                                <input type="text" class="cost opc" id="caja_h" name="caja_h" placeholder="$0.00"/>
                            </td>

                            <td>
                                <input type="text" class="price iv" id="precio_u4" name="precio_u4" disabled readonly placeholder="$0.00"/>
                            </td>

                            <td>
                                <input type="text" class="iva" id="total3" name="total3" disabled readonly placeholder="$0.00"/>
                            </td>
                        </tr>

                        <tr class="item-row">
                            <td class="item-name">
                                <div >
                                    <strong>Gancho de arrastre</strong>
                                </div>
                            </td>

                            <td>
                                <input type="text" class="cost opc" id="caja_h" name="caja_h" placeholder="$0.00"/>
                            </td>

                            <td>
                                <input type="text" class="price iv" id="precio_u4" name="precio_u4" disabled readonly placeholder="$0.00"/>
                            </td>

                            <td>
                                <input type="text" class="iva" id="total3" name="total3" disabled readonly placeholder="$0.00"/>
                            </td>
                        </tr>
                        <tr class="item-row">
                            <td class="item-name">
                                <div >
                                    <strong>Autoinflado 2 ejes</strong>
                                </div>
                            </td>

                            <td>
                                <input type="text" class="cost opc" id="autoinflado_2" name="autoinflado_2" placeholder="$0.00"/>
                            </td>

                            <td>
                                <input type="text" class="price iv" id="precio_u6" name="precio_u6" disabled readonly placeholder="$0.00"/>
                            </td>

                            <td>
                                <input type="text" class="iva" id="total5" name="total5" disabled readonly placeholder="$0.00"/>
                            </td>
                        </tr>
                        <tr class="item-row">
                            <td class="item-name">
                                <div >
                                    <strong>Autoinflado 3 ejes</strong>
                                </div>
                            </td>

                            <td>
                                <input type="text" class="cost opc" id="autoinflado_3" name="autoinflado_3" placeholder="$0.00"/>
                            </td>

                            <td>
                                <input type="text" class="price iv" id="precio_u7" name="precio_u7" disabled readonly placeholder="$0.00"/>
                            </td>

                            <td>
                                <input type="text" class="iva" id="total6" name="total6" disabled readonly placeholder="$0.00"/>
                            </td>
                        </tr>

                                   <?php
                                   //opcionales
                                }elseif ($_GET['id'] == 2) {
                                    ?>
                                    <tr class="item-row">
                            <td class="item-name">
                                <div>
                                    <strong>Patin Extra</strong>
                                </div>
                            </td>

                            <td>
                                <input type="text" class="cost opc" id="patin_ext" name="patin_ext" placeholder="$0.00"/>
                            </td>

                            <td>
                                <input type="text" class="price iv" id="precio_u1" name="precio_u1" disabled readonly placeholder="$0.00"/>
                            </td>
                            <td>
                                <input type="text" class="iva" id="total" name="total" disabled readonly placeholder="$0.00"/>
                            </td>
                        </tr>

                        <tr class="item-row">
                            <td class="item-name">
                                <div>
                                    <strong>Autoinflado 2 ejes</strong>
                                </div>
                            </td>

                            <td>
                                <input type="text" class="cost opc" id="patin_ext" name="patin_ext" placeholder="$0.00"/>
                            </td>

                            <td>
                                <input type="text" class="price iv" id="precio_u1" name="precio_u1" disabled readonly placeholder="$0.00"/>
                            </td>
                            <td>
                                <input type="text" class="iva" id="total" name="total" disabled readonly placeholder="$0.00"/>
                            </td>
                        </tr>

                        <tr class="item-row">
                            <td class="item-name">
                                <div>
                                    <strong>Autoinflado 3 ejes</strong>
                                </div>
                            </td>

                            <td>
                                <input type="text" class="cost opc" id="patin_ext" name="patin_ext" placeholder="$0.00"/>
                            </td>

                            <td>
                                <input type="text" class="price iv" id="precio_u1" name="precio_u1" disabled readonly placeholder="$0.00"/>
                            </td>
                            <td>
                                <input type="text" class="iva" id="total" name="total" disabled readonly placeholder="$0.00"/>
                            </td>
                        </tr>

                        <tr class="item-row">
                            <td class="item-name">
                                <div>
                                    <strong>Rectráctil de un eje</strong>
                                </div>
                            </td>

                            <td>
                                <input type="text" class="cost opc" id="patin_ext" name="patin_ext" placeholder="$0.00"/>
                            </td>

                            <td>
                                <input type="text" class="price iv" id="precio_u1" name="precio_u1" disabled readonly placeholder="$0.00"/>
                            </td>
                            <td>
                                <input type="text" class="iva" id="total" name="total" disabled readonly placeholder="$0.00"/>
                            </td>
                        </tr>

                        <tr class="item-row">
                            <td class="item-name">
                                <div>
                                    <strong>Rectráctil de dos ejes</strong>
                                </div>
                            </td>

                            <td>
                                <input type="text" class="cost opc" id="patin_ext" name="patin_ext" placeholder="$0.00"/>
                            </td>

                            <td>
                                <input type="text" class="price iv" id="precio_u1" name="precio_u1" disabled readonly placeholder="$0.00"/>
                            </td>
                            <td>
                                <input type="text" class="iva" id="total" name="total" disabled readonly placeholder="$0.00"/>
                            </td>
                        </tr>

                        <tr class="item-row">
                            <td class="item-name">
                                <div>
                                    <strong>Sistema antivolcadura</strong>
                                </div>
                            </td>

                            <td>
                                <input type="text" class="cost opc" id="patin_ext" name="patin_ext" placeholder="$0.00"/>
                            </td>

                            <td>
                                <input type="text" class="price iv" id="precio_u1" name="precio_u1" disabled readonly placeholder="$0.00"/>
                            </td>
                            <td>
                                <input type="text" class="iva" id="total" name="total" disabled readonly placeholder="$0.00"/>
                            </td>
                        </tr>
                                    <?php
                                } elseif ($_GET['id'] == 3) {
                            ?>  
                        <tr class="item-row">
                            <td class="item-name">
                                <div >
                                    <strong>Autoinflado 2 ejes</strong>
                                </div>
                            </td>

                            <td>
                                <input type="text" class="cost opc" id="autoinflado_2" name="autoinflado_2" placeholder="$0.00"/>
                            </td>

                            <td>
                                <input type="text" class="price iv" id="precio_u6" name="precio_u6" disabled readonly placeholder="$0.00"/>
                            </td>

                            <td>
                                <input type="text" class="iva" id="total5" name="total5" disabled readonly placeholder="$0.00"/>
                            </td>
                        </tr>
                                    <?php
                                }elseif ($_GET['id'] == 4) {
                                    ?>
                                    <tr class="item-row">
                            <td class="item-name">
                                <div >
                                    <strong>Patin Extra</strong>
                                </div>
                            </td>

                            <td>
                                <input type="text" class="cost opc" id="autoinflado_2" name="autoinflado_2" placeholder="$0.00"/>
                            </td>

                            <td>
                                <input type="text" class="price iv" id="precio_u6" name="precio_u6" disabled readonly placeholder="$0.00"/>
                            </td>

                            <td>
                                <input type="text" class="iva" id="total5" name="total5" disabled readonly placeholder="$0.00"/>
                            </td>
                        </tr>

                        <tr class="item-row">
                            <td class="item-name">
                                <div >
                                    <strong>Riel Logístico</strong>
                                </div>
                            </td>

                            <td>
                                <input type="text" class="cost opc" id="autoinflado_2" name="autoinflado_2" placeholder="$0.00"/>
                            </td>

                            <td>
                                <input type="text" class="price iv" id="precio_u6" name="precio_u6" disabled readonly placeholder="$0.00"/>
                            </td>

                            <td>
                                <input type="text" class="iva" id="total5" name="total5" disabled readonly placeholder="$0.00"/>
                            </td>
                        </tr>

                        <tr class="item-row">
                            <td class="item-name">
                                <div >
                                    <strong>Autoinflado 1 ejes</strong>
                                </div>
                            </td>

                            <td>
                                <input type="text" class="cost opc" id="autoinflado_2" name="autoinflado_2" placeholder="$0.00"/>
                            </td>

                            <td>
                                <input type="text" class="price iv" id="precio_u6" name="precio_u6" disabled readonly placeholder="$0.00"/>
                            </td>

                            <td>
                                <input type="text" class="iva" id="total5" name="total5" disabled readonly placeholder="$0.00"/>
                            </td>
                        </tr>

                        <tr class="item-row">
                            <td class="item-name">
                                <div >
                                    <strong>Autoinflado 2 ejes</strong>
                                </div>
                            </td>

                            <td>
                                <input type="text" class="cost opc" id="autoinflado_2" name="autoinflado_2" placeholder="$0.00"/>
                            </td>

                            <td>
                                <input type="text" class="price iv" id="precio_u6" name="precio_u6" disabled readonly placeholder="$0.00"/>
                            </td>

                            <td>
                                <input type="text" class="iva" id="total5" name="total5" disabled readonly placeholder="$0.00"/>
                            </td>
                        </tr>

                        <tr class="item-row">
                            <td class="item-name">
                                <div >
                                    <strong>Autoinflado 3 ejes</strong>
                                </div>
                            </td>

                            <td>
                                <input type="text" class="cost opc" id="autoinflado_2" name="autoinflado_2" placeholder="$0.00"/>
                            </td>

                            <td>
                                <input type="text" class="price iv" id="precio_u6" name="precio_u6" disabled readonly placeholder="$0.00"/>
                            </td>

                            <td>
                                <input type="text" class="iva" id="total5" name="total5" disabled readonly placeholder="$0.00"/>
                            </td>
                        </tr>

                        <tr class="item-row">
                            <td class="item-name">
                                <div >
                                    <strong>Interiores en linner</strong>
                                </div>
                            </td>

                            <td>
                                <input type="text" class="cost opc" id="autoinflado_2" name="autoinflado_2" placeholder="$0.00"/>
                            </td>

                            <td>
                                <input type="text" class="price iv" id="precio_u6" name="precio_u6" disabled readonly placeholder="$0.00"/>
                            </td>

                            <td>
                                <input type="text" class="iva" id="total5" name="total5" disabled readonly placeholder="$0.00"/>
                            </td>
                        </tr>
                        <tr class="item-row">
                            <td class="item-name">
                                <div >
                                    <strong>Puerta cortina</strong>
                                </div>
                            </td>

                            <td>
                                <input type="text" class="cost opc" id="autoinflado_2" name="autoinflado_2" placeholder="$0.00"/>
                            </td>

                            <td>
                                <input type="text" class="price iv" id="precio_u6" name="precio_u6" disabled readonly placeholder="$0.00"/>
                            </td>

                            <td>
                                <input type="text" class="iva" id="total5" name="total5" disabled readonly placeholder="$0.00"/>
                            </td>
                        </tr>
                        <tr class="item-row">
                            <td class="item-name">
                                <div >
                                    <strong>Rectráctil de un eje</strong>
                                </div>
                            </td>

                            <td>
                                <input type="text" class="cost opc" id="autoinflado_2" name="autoinflado_2" placeholder="$0.00"/>
                            </td>

                            <td>
                                <input type="text" class="price iv" id="precio_u6" name="precio_u6" disabled readonly placeholder="$0.00"/>
                            </td>

                            <td>
                                <input type="text" class="iva" id="total5" name="total5" disabled readonly placeholder="$0.00"/>
                            </td>
                        </tr>
                        <tr class="item-row">
                            <td class="item-name">
                                <div >
                                    <strong>Gancho de arrastre</strong>
                                </div>
                            </td>

                            <td>
                                <input type="text" class="cost opc" id="autoinflado_2" name="autoinflado_2" placeholder="$0.00"/>
                            </td>

                            <td>
                                <input type="text" class="price iv" id="precio_u6" name="precio_u6" disabled readonly placeholder="$0.00"/>
                            </td>

                            <td>
                                <input type="text" class="iva" id="total5" name="total5" disabled readonly placeholder="$0.00"/>
                            </td>
                        </tr>
                        <tr class="item-row">
                            <td class="item-name">
                                <div >
                                    <strong>Piso antiderrapante</strong>
                                </div>
                            </td>

                            <td>
                                <input type="text" class="cost opc" id="autoinflado_2" name="autoinflado_2" placeholder="$0.00"/>
                            </td>

                            <td>
                                <input type="text" class="price iv" id="precio_u6" name="precio_u6" disabled readonly placeholder="$0.00"/>
                            </td>

                            <td>
                                <input type="text" class="iva" id="total5" name="total5" disabled readonly placeholder="$0.00"/>
                            </td>
                        </tr>
                        <tr class="item-row">
                            <td class="item-name">
                                <div >
                                    <strong>Puerta lateral</strong>
                                </div>
                            </td>

                            <td>
                                <input type="text" class="cost opc" id="autoinflado_2" name="autoinflado_2" placeholder="$0.00"/>
                            </td>

                            <td>
                                <input type="text" class="price iv" id="precio_u6" name="precio_u6" disabled readonly placeholder="$0.00"/>
                            </td>

                            <td>
                                <input type="text" class="iva" id="total5" name="total5" disabled readonly placeholder="$0.00"/>
                            </td>
                        </tr>
                        <tr class="item-row">
                            <td class="item-name">
                                <div >
                                    <strong>Sistema antivolcadura</strong>
                                </div>
                            </td>

                            <td>
                                <input type="text" class="cost opc" id="autoinflado_2" name="autoinflado_2" placeholder="$0.00"/>
                            </td>

                            <td>
                                <input type="text" class="price iv" id="precio_u6" name="precio_u6" disabled readonly placeholder="$0.00"/>
                            </td>

                            <td>
                                <input type="text" class="iva" id="total5" name="total5" disabled readonly placeholder="$0.00"/>
                            </td>
                        </tr>
                        <tr class="item-row">
                            <td class="item-name">
                                <div >
                                    <strong>Ventilas</strong>
                                </div>
                            </td>

                            <td>
                                <input type="text" class="cost opc" id="autoinflado_2" name="autoinflado_2" placeholder="$0.00"/>
                            </td>

                            <td>
                                <input type="text" class="price iv" id="precio_u6" name="precio_u6" disabled readonly placeholder="$0.00"/>
                            </td>

                            <td>
                                <input type="text" class="iva" id="total5" name="total5" disabled readonly placeholder="$0.00"/>
                            </td>
                        </tr>
                        <tr class="item-row">
                            <td class="item-name">
                                <div >
                                    <strong>Interior DURAPLATE</strong>
                                </div>
                            </td>

                            <td>
                                <input type="text" class="cost opc" id="autoinflado_2" name="autoinflado_2" placeholder="$0.00"/>
                            </td>

                            <td>
                                <input type="text" class="price iv" id="precio_u6" name="precio_u6" disabled readonly placeholder="$0.00"/>
                            </td>

                            <td>
                                <input type="text" class="iva" id="total5" name="total5" disabled readonly placeholder="$0.00"/>
                            </td>
                        </tr>
                                    <?php
                                }elseif ($_GET['id'] == 5) {
                                    ?>
                                    <tr class="item-row">
                            <td class="item-name">
                                <div>
                                    <strong>Patin Extra</strong>
                                </div>
                            </td>

                            <td>
                                <input type="text" class="cost opc" id="patin_ext" name="patin_ext" placeholder="$0.00"/>
                            </td>

                            <td>
                                <input type="text" class="price iv" id="precio_u1" name="precio_u1" disabled readonly placeholder="$0.00"/>
                            </td>
                            <td>
                                <input type="text" class="iva" id="total" name="total" disabled readonly placeholder="$0.00"/>
                            </td>
                        </tr>

                        <tr class="item-row">
                            <td class="item-name">
                                <div >
                                    <strong>Retractil 1 eje</strong>
                                </div>
                            </td>

                            <td>
                                <input type="text" class="cost opc" id="retractil_1" name="retractil_1" placeholder="$0.00"/>
                            </td>

                            <td>
                                <input type="text" class="price iv" id="precio_u2" name="precio_u2" disabled readonly placeholder="$0.00"/>
                            </td>

                            <td>
                                <input type="text" class="iva" id="total1" name="total1" disabled readonly placeholder="$0.00"/>
                            </td>
                        </tr>
                        <tr class="item-row">
                            <td class="item-name">
                                <div >
                                    <strong>Retractil 2 ejes</strong>
                                </div>
                            </td>

                            <td>
                                <input type="text" class="cost opc" id="retractil_2" name="retractil_2" placeholder="$0.00"/>
                            </td>

                            <td>
                                <input type="text" class="price iv" id="precio_u3" name="precio_u3" disabled readonly placeholder="$0.00"/>
                            </td>

                            <td>
                                <input type="text" class="iva" id="total2" name="total2" disabled readonly placeholder="$0.00"/>
                            </td>
                        </tr>

                        <tr class="item-row">
                            <td class="item-name">
                                <div >
                                    <strong>Caja de herramientas</strong>
                                </div>
                            </td>

                            <td>
                                <input type="text" class="cost opc" id="caja_h" name="caja_h" placeholder="$0.00"/>
                            </td>

                            <td>
                                <input type="text" class="price iv" id="precio_u4" name="precio_u4" disabled readonly placeholder="$0.00"/>
                            </td>

                            <td>
                                <input type="text" class="iva" id="total3" name="total3" disabled readonly placeholder="$0.00"/>
                            </td>
                        </tr>

                        <tr class="item-row">
                            <td class="item-name">
                                <div >
                                    <strong>Gancho de arrastre</strong>
                                </div>
                            </td>

                            <td>
                                <input type="text" class="cost opc" id="gancho_holland" name="gancho_holland" placeholder="$0.00"/>
                            </td>

                            <td>
                                <input type="text" class="price iv" id="precio_u5" name="precio_u5" disabled readonly placeholder="$0.00"/>
                            </td>

                            <td>
                                <input type="text" class="iva" id="total4" name="total4" disabled readonly placeholder="$0.00"/>
                            </td>
                        </tr>

                        <tr class="item-row">
                            <td class="item-name">
                                <div >
                                    <strong>Autoinflado 2 ejes</strong>
                                </div>
                            </td>

                            <td>
                                <input type="text" class="cost opc" id="autoinflado_2" name="autoinflado_2" placeholder="$0.00"/>
                            </td>

                            <td>
                                <input type="text" class="price iv" id="precio_u6" name="precio_u6" disabled readonly placeholder="$0.00"/>
                            </td>

                            <td>
                                <input type="text" class="iva" id="total5" name="total5" disabled readonly placeholder="$0.00"/>
                            </td>
                        </tr>
                        <tr class="item-row">
                            <td class="item-name">
                                <div >
                                    <strong>Autoinflado 3 ejes</strong>
                                </div>
                            </td>

                            <td>
                                <input type="text" class="cost opc" id="autoinflado_3" name="autoinflado_3" placeholder="$0.00"/>
                            </td>

                            <td>
                                <input type="text" class="price iv" id="precio_u7" name="precio_u7" disabled readonly placeholder="$0.00"/>
                            </td>

                            <td>
                                <input type="text" class="iva" id="total6" name="total6" disabled readonly placeholder="$0.00"/>
                            </td>
                        </tr>
                        <tr class="item-row">
                                <td class="item-name">
                                    <div >
                                        <strong>Piso antiderrapante</strong>
                                    </div>
                                </td>

                                <td>
                                    <input type="text" class="cost opc" id="piso_antiderrapante" name="piso_antiderrapante" placeholder="$0.00"/>
                                </td>

                                <td>
                                    <input type="text" class="price iv" id="precio_u10" name="precio_u10" disabled readonly placeholder="$0.00"/>
                                </td>

                                <td>
                                    <input type="text" class="iva" id="total9" name="total9" disabled readonly placeholder="$0.00"/>
                                </td>
                            </tr>
                            <tr class="item-row">
                                <td class="item-name">
                                    <div >
                                        <strong>Winches</strong>
                                    </div>
                                </td>

                                <td>
                                    <input type="text" class="cost opc" id="winches" name="winches" placeholder="$0.00"/>
                                </td>

                                <td>
                                    <input type="text" class="price iv" id="precio_u14" name="precio_u14" disabled readonly placeholder="$0.00"/>
                                </td>

                                <td>
                                    <input type="text" class="iva" id="total9" name="total13" disabled readonly placeholder="$0.00"/>
                                </td>
                            </tr>
                                    <?php
                                }elseif ($_GET['id'] == 6) {
                                    ?>
                                    <tr class="item-row">
                            <td class="item-name">
                                <div>
                                    <strong>Patin Extra</strong>
                                </div>
                            </td>

                            <td>
                                <input type="text" class="cost opc" id="patin_ext" name="patin_ext" placeholder="$0.00"/>
                            </td>

                            <td>
                                <input type="text" class="price iv" id="precio_u1" name="precio_u1" disabled readonly placeholder="$0.00"/>
                            </td>
                            <td>
                                <input type="text" class="iva" id="total" name="total" disabled readonly placeholder="$0.00"/>
                            </td>
                        </tr>

                        <tr class="item-row">
                            <td class="item-name">
                                <div >
                                    <strong>Retractil 1 eje</strong>
                                </div>
                            </td>

                            <td>
                                <input type="text" class="cost opc" id="retractil_1" name="retractil_1" placeholder="$0.00"/>
                            </td>

                            <td>
                                <input type="text" class="price iv" id="precio_u2" name="precio_u2" disabled readonly placeholder="$0.00"/>
                            </td>

                            <td>
                                <input type="text" class="iva" id="total1" name="total1" disabled readonly placeholder="$0.00"/>
                            </td>
                        </tr>
                
                        <tr class="item-row">
                            <td class="item-name">
                                <div >
                                    <strong>Caja de herramientas</strong>
                                </div>
                            </td>

                            <td>
                                <input type="text" class="cost opc" id="caja_h" name="caja_h" placeholder="$0.00"/>
                            </td>

                            <td>
                                <input type="text" class="price iv" id="precio_u4" name="precio_u4" disabled readonly placeholder="$0.00"/>
                            </td>

                            <td>
                                <input type="text" class="iva" id="total3" name="total3" disabled readonly placeholder="$0.00"/>
                            </td>
                        </tr>


                        <tr class="item-row">
                            <td class="item-name">
                                <div >
                                    <strong>Autoinflado 2 ejes</strong>
                                </div>
                            </td>

                            <td>
                                <input type="text" class="cost opc" id="autoinflado_2" name="autoinflado_2" placeholder="$0.00"/>
                            </td>

                            <td>
                                <input type="text" class="price iv" id="precio_u6" name="precio_u6" disabled readonly placeholder="$0.00"/>
                            </td>

                            <td>
                                <input type="text" class="iva" id="total5" name="total5" disabled readonly placeholder="$0.00"/>
                            </td>
                        </tr>
                        
            
                            <tr class="item-row">
                                <td class="item-name">
                                    <div >
                                        <strong>Sistema antivolcadura</strong>
                                    </div>
                                </td>

                                <td>
                                    <input type="text" class="cost opc" id="winches" name="winches" placeholder="$0.00"/>
                                </td>

                                <td>
                                    <input type="text" class="price iv" id="precio_u14" name="precio_u14" disabled readonly placeholder="$0.00"/>
                                </td>

                                <td>
                                    <input type="text" class="iva" id="total9" name="total13" disabled readonly placeholder="$0.00"/>
                                </td>
                            </tr>
                                    <?php
                                }elseif ($_GET['id'] == 7) {
                                     ?>
                                    <tr class="item-row">
                            <td class="item-name">
                                <div>
                                    <strong>Patin Extra</strong>
                                </div>
                            </td>

                            <td>
                                <input type="text" class="cost opc" id="patin_ext" name="patin_ext" placeholder="$0.00"/>
                            </td>

                            <td>
                                <input type="text" class="price iv" id="precio_u1" name="precio_u1" disabled readonly placeholder="$0.00"/>
                            </td>
                            <td>
                                <input type="text" class="iva" id="total" name="total" disabled readonly placeholder="$0.00"/>
                            </td>
                        </tr>

                        <tr class="item-row">
                            <td class="item-name">
                                <div >
                                    <strong>Retractil 1 eje</strong>
                                </div>
                            </td>

                            <td>
                                <input type="text" class="cost opc" id="retractil_1" name="retractil_1" placeholder="$0.00"/>
                            </td>

                            <td>
                                <input type="text" class="price iv" id="precio_u2" name="precio_u2" disabled readonly placeholder="$0.00"/>
                            </td>

                            <td>
                                <input type="text" class="iva" id="total1" name="total1" disabled readonly placeholder="$0.00"/>
                            </td>
                        </tr>
                
                        <tr class="item-row">
                            <td class="item-name">
                                <div >
                                    <strong>Caja de herramientas</strong>
                                </div>
                            </td>

                            <td>
                                <input type="text" class="cost opc" id="caja_h" name="caja_h" placeholder="$0.00"/>
                            </td>

                            <td>
                                <input type="text" class="price iv" id="precio_u4" name="precio_u4" disabled readonly placeholder="$0.00"/>
                            </td>

                            <td>
                                <input type="text" class="iva" id="total3" name="total3" disabled readonly placeholder="$0.00"/>
                            </td>
                        </tr>


                        <tr class="item-row">
                            <td class="item-name">
                                <div >
                                    <strong>Autoinflado 2 ejes</strong>
                                </div>
                            </td>

                            <td>
                                <input type="text" class="cost opc" id="autoinflado_2" name="autoinflado_2" placeholder="$0.00"/>
                            </td>

                            <td>
                                <input type="text" class="price iv" id="precio_u6" name="precio_u6" disabled readonly placeholder="$0.00"/>
                            </td>

                            <td>
                                <input type="text" class="iva" id="total5" name="total5" disabled readonly placeholder="$0.00"/>
                            </td>
                        </tr>
                        
            
                            <tr class="item-row">
                                <td class="item-name">
                                    <div >
                                        <strong>Sistema antivolcadura</strong>
                                    </div>
                                </td>

                                <td>
                                    <input type="text" class="cost opc" id="winches" name="winches" placeholder="$0.00"/>
                                </td>

                                <td>
                                    <input type="text" class="price iv" id="precio_u14" name="precio_u14" disabled readonly placeholder="$0.00"/>
                                </td>

                                <td>
                                    <input type="text" class="iva" id="total9" name="total13" disabled readonly placeholder="$0.00"/>
                                </td>
                            </tr>
                                    <?php
                                }elseif ($_GET['id'] == 8) {
                                     ?>
                                    <tr class="item-row">
                            <td class="item-name">
                                <div>
                                    <strong>Patin Extra</strong>
                                </div>
                            </td>

                            <td>
                                <input type="text" class="cost opc" id="patin_ext" name="patin_ext" placeholder="$0.00"/>
                            </td>

                            <td>
                                <input type="text" class="price iv" id="precio_u1" name="precio_u1" disabled readonly placeholder="$0.00"/>
                            </td>
                            <td>
                                <input type="text" class="iva" id="total" name="total" disabled readonly placeholder="$0.00"/>
                            </td>
                        </tr>

                        <tr class="item-row">
                            <td class="item-name">
                                <div >
                                    <strong>Retractil 1 eje</strong>
                                </div>
                            </td>

                            <td>
                                <input type="text" class="cost opc" id="retractil_1" name="retractil_1" placeholder="$0.00"/>
                            </td>

                            <td>
                                <input type="text" class="price iv" id="precio_u2" name="precio_u2" disabled readonly placeholder="$0.00"/>
                            </td>

                            <td>
                                <input type="text" class="iva" id="total1" name="total1" disabled readonly placeholder="$0.00"/>
                            </td>
                        </tr>
                
                        <tr class="item-row">
                            <td class="item-name">
                                <div >
                                    <strong>Caja de herramientas</strong>
                                </div>
                            </td>

                            <td>
                                <input type="text" class="cost opc" id="caja_h" name="caja_h" placeholder="$0.00"/>
                            </td>

                            <td>
                                <input type="text" class="price iv" id="precio_u4" name="precio_u4" disabled readonly placeholder="$0.00"/>
                            </td>

                            <td>
                                <input type="text" class="iva" id="total3" name="total3" disabled readonly placeholder="$0.00"/>
                            </td>
                        </tr>


                        <tr class="item-row">
                            <td class="item-name">
                                <div >
                                    <strong>Autoinflado 2 ejes</strong>
                                </div>
                            </td>

                            <td>
                                <input type="text" class="cost opc" id="autoinflado_2" name="autoinflado_2" placeholder="$0.00"/>
                            </td>

                            <td>
                                <input type="text" class="price iv" id="precio_u6" name="precio_u6" disabled readonly placeholder="$0.00"/>
                            </td>

                            <td>
                                <input type="text" class="iva" id="total5" name="total5" disabled readonly placeholder="$0.00"/>
                            </td>
                        </tr>
                        
            
                            <tr class="item-row">
                                <td class="item-name">
                                    <div >
                                        <strong>Sistema antivolcadura</strong>
                                    </div>
                                </td>

                                <td>
                                    <input type="text" class="cost opc" id="winches" name="winches" placeholder="$0.00"/>
                                </td>

                                <td>
                                    <input type="text" class="price iv" id="precio_u14" name="precio_u14" disabled readonly placeholder="$0.00"/>
                                </td>

                                <td>
                                    <input type="text" class="iva" id="total9" name="total13" disabled readonly placeholder="$0.00"/>
                                </td>
                            </tr>
                                    <?php
                                }else if ($_GET['id'] == 9) {
                                   ?>
                                   <tr class="item-row">
                            <td class="item-name">
                                <div>
                                    <strong>Patin Extra</strong>
                                </div>
                            </td>

                            <td>
                                <input type="text" class="cost opc" id="patin_ext" name="patin_ext" placeholder="$0.00"/>
                            </td>

                            <td>
                                <input type="text" class="price iv" id="precio_u1" name="precio_u1" disabled readonly placeholder="$0.00"/>
                            </td>
                            <td>
                                <input type="text" class="iva" id="total" name="total" disabled readonly placeholder="$0.00"/>
                            </td>
                        </tr>

                        <tr class="item-row">
                            <td class="item-name">
                                <div >
                                    <strong>Retractil 1 eje</strong>
                                </div>
                            </td>

                            <td>
                                <input type="text" class="cost opc" id="retractil_1" name="retractil_1" placeholder="$0.00"/>
                            </td>

                            <td>
                                <input type="text" class="price iv" id="precio_u2" name="precio_u2" disabled readonly placeholder="$0.00"/>
                            </td>

                            <td>
                                <input type="text" class="iva" id="total1" name="total1" disabled readonly placeholder="$0.00"/>
                            </td>
                        </tr>
                        <tr class="item-row">
                            <td class="item-name">
                                <div >
                                    <strong>Retractil 2 ejes</strong>
                                </div>
                            </td>

                            <td>
                                <input type="text" class="cost opc" id="retractil_2" name="retractil_2" placeholder="$0.00"/>
                            </td>

                            <td>
                                <input type="text" class="price iv" id="precio_u3" name="precio_u3" disabled readonly placeholder="$0.00"/>
                            </td>

                            <td>
                                <input type="text" class="iva" id="total2" name="total2" disabled readonly placeholder="$0.00"/>
                            </td>
                        </tr>

                        <tr class="item-row">
                            <td class="item-name">
                                <div >
                                    <strong>Caja de herramientas</strong>
                                </div>
                            </td>

                            <td>
                                <input type="text" class="cost opc" id="caja_h" name="caja_h" placeholder="$0.00"/>
                            </td>

                            <td>
                                <input type="text" class="price iv" id="precio_u4" name="precio_u4" disabled readonly placeholder="$0.00"/>
                            </td>

                            <td>
                                <input type="text" class="iva" id="total3" name="total3" disabled readonly placeholder="$0.00"/>
                            </td>
                        </tr>

                        <tr class="item-row">
                            <td class="item-name">
                                <div >
                                    <strong>Gancho de arrastre</strong>
                                </div>
                            </td>

                            <td>
                                <input type="text" class="cost opc" id="gancho_holland" name="gancho_holland" placeholder="$0.00"/>
                            </td>

                            <td>
                                <input type="text" class="price iv" id="precio_u5" name="precio_u5" disabled readonly placeholder="$0.00"/>
                            </td>

                            <td>
                                <input type="text" class="iva" id="total4" name="total4" disabled readonly placeholder="$0.00"/>
                            </td>
                        </tr>

                        <tr class="item-row">
                            <td class="item-name">
                                <div >
                                    <strong>Autoinflado 2 ejes</strong>
                                </div>
                            </td>

                            <td>
                                <input type="text" class="cost opc" id="autoinflado_2" name="autoinflado_2" placeholder="$0.00"/>
                            </td>

                            <td>
                                <input type="text" class="price iv" id="precio_u6" name="precio_u6" disabled readonly placeholder="$0.00"/>
                            </td>

                            <td>
                                <input type="text" class="iva" id="total5" name="total5" disabled readonly placeholder="$0.00"/>
                            </td>
                        </tr>
                        <tr class="item-row">
                            <td class="item-name">
                                <div >
                                    <strong>Autoinflado 3 ejes</strong>
                                </div>
                            </td>

                            <td>
                                <input type="text" class="cost opc" id="autoinflado_3" name="autoinflado_3" placeholder="$0.00"/>
                            </td>

                            <td>
                                <input type="text" class="price iv" id="precio_u7" name="precio_u7" disabled readonly placeholder="$0.00"/>
                            </td>

                            <td>
                                <input type="text" class="iva" id="total6" name="total6" disabled readonly placeholder="$0.00"/>
                            </td>
                        </tr>

                                   <?php
                                   //opcionales
                                }else if ($_GET['id'] == 10) {
                                   ?>
                                   <tr class="item-row">
                            <td class="item-name">
                                <div>
                                    <strong>Patin Extra</strong>
                                </div>
                            </td>

                            <td>
                                <input type="text" class="cost opc" id="patin_ext" name="patin_ext" placeholder="$0.00"/>
                            </td>

                            <td>
                                <input type="text" class="price iv" id="precio_u1" name="precio_u1" disabled readonly placeholder="$0.00"/>
                            </td>
                            <td>
                                <input type="text" class="iva" id="total" name="total" disabled readonly placeholder="$0.00"/>
                            </td>
                        </tr>

                        <tr class="item-row">
                            <td class="item-name">
                                <div >
                                    <strong>Retractil 1 eje</strong>
                                </div>
                            </td>

                            <td>
                                <input type="text" class="cost opc" id="retractil_1" name="retractil_1" placeholder="$0.00"/>
                            </td>

                            <td>
                                <input type="text" class="price iv" id="precio_u2" name="precio_u2" disabled readonly placeholder="$0.00"/>
                            </td>

                            <td>
                                <input type="text" class="iva" id="total1" name="total1" disabled readonly placeholder="$0.00"/>
                            </td>
                        </tr>
                        <tr class="item-row">
                            <td class="item-name">
                                <div >
                                    <strong>Retractil 2 ejes</strong>
                                </div>
                            </td>

                            <td>
                                <input type="text" class="cost opc" id="retractil_2" name="retractil_2" placeholder="$0.00"/>
                            </td>

                            <td>
                                <input type="text" class="price iv" id="precio_u3" name="precio_u3" disabled readonly placeholder="$0.00"/>
                            </td>

                            <td>
                                <input type="text" class="iva" id="total2" name="total2" disabled readonly placeholder="$0.00"/>
                            </td>
                        </tr>

                        <tr class="item-row">
                            <td class="item-name">
                                <div >
                                    <strong>Caja de herramientas</strong>
                                </div>
                            </td>

                            <td>
                                <input type="text" class="cost opc" id="caja_h" name="caja_h" placeholder="$0.00"/>
                            </td>

                            <td>
                                <input type="text" class="price iv" id="precio_u4" name="precio_u4" disabled readonly placeholder="$0.00"/>
                            </td>

                            <td>
                                <input type="text" class="iva" id="total3" name="total3" disabled readonly placeholder="$0.00"/>
                            </td>
                        </tr><tr class="item-row">
                            <td class="item-name">
                                <div >
                                    <strong>Gancho de arrastre</strong>
                                </div>
                            </td>

                            <td>
                                <input type="text" class="cost opc" id="caja_h" name="caja_h" placeholder="$0.00"/>
                            </td>

                            <td>
                                <input type="text" class="price iv" id="precio_u4" name="precio_u4" disabled readonly placeholder="$0.00"/>
                            </td>

                            <td>
                                <input type="text" class="iva" id="total3" name="total3" disabled readonly placeholder="$0.00"/>
                            </td>
                        </tr>

                        <tr class="item-row">
                            <td class="item-name">
                                <div >
                                    <strong>Piso antiderrapante</strong>
                                </div>
                            </td>

                            <td>
                                <input type="text" class="cost opc" id="caja_h" name="caja_h" placeholder="$0.00"/>
                            </td>

                            <td>
                                <input type="text" class="price iv" id="precio_u4" name="precio_u4" disabled readonly placeholder="$0.00"/>
                            </td>

                            <td>
                                <input type="text" class="iva" id="total3" name="total3" disabled readonly placeholder="$0.00"/>
                            </td>
                        </tr>

                        <tr class="item-row">
                            <td class="item-name">
                                <div >
                                    <strong>Winches</strong>
                                </div>
                            </td>

                            <td>
                                <input type="text" class="cost opc" id="gancho_holland" name="gancho_holland" placeholder="$0.00"/>
                            </td>

                            <td>
                                <input type="text" class="price iv" id="precio_u5" name="precio_u5" disabled readonly placeholder="$0.00"/>
                            </td>

                            <td>
                                <input type="text" class="iva" id="total4" name="total4" disabled readonly placeholder="$0.00"/>
                            </td>
                        </tr>

                        <tr class="item-row">
                            <td class="item-name">
                                <div >
                                    <strong>Autoinflado 2 ejes</strong>
                                </div>
                            </td>

                            <td>
                                <input type="text" class="cost opc" id="autoinflado_2" name="autoinflado_2" placeholder="$0.00"/>
                            </td>

                            <td>
                                <input type="text" class="price iv" id="precio_u6" name="precio_u6" disabled readonly placeholder="$0.00"/>
                            </td>

                            <td>
                                <input type="text" class="iva" id="total5" name="total5" disabled readonly placeholder="$0.00"/>
                            </td>
                        </tr>
                        <tr class="item-row">
                            <td class="item-name">
                                <div >
                                    <strong>Autoinflado 3 ejes</strong>
                                </div>
                            </td>

                            <td>
                                <input type="text" class="cost opc" id="autoinflado_3" name="autoinflado_3" placeholder="$0.00"/>
                            </td>

                            <td>
                                <input type="text" class="price iv" id="precio_u7" name="precio_u7" disabled readonly placeholder="$0.00"/>
                            </td>

                            <td>
                                <input type="text" class="iva" id="total6" name="total6" disabled readonly placeholder="$0.00"/>
                            </td>
                        </tr>

                                   <?php
                               }
                            ?>

                        <tr>
                            <td colspan="4">
                                <strong style="font-size:25px;">Totales</strong>
                            </td>
                        </tr>

                        <tr>
                            <td   class="item-name">
                                <strong>Precio</strong>
                            </td>

                            <td class="total-value">
                                <div id="subtotal">
                                    <input type="number" id="precio_cotiza" name="cantidad_precio" placeholder="$0.00" disabled readonly />
                                </div>
                            </td>
                            <td  class="blank"></td>
                            <td  class="blank"></td>

                        <tr>
                            <td  class="item-name">
                                <strong>Total Opcionales</strong>
                            </td>
                            <td class="total-value">
                                <div id="total">
                                    <input type="number" id="total_opcionales" name="total_opcionales" placeholder="$0.00" disabled readonly />
                                </div>
                            </td>
                            <td  class="blank"></td>
                            <td  class="blank"></td>
                        </tr>
                        <tr>
                            <td  class="item-name">
                                <strong>Total IVA</strong>
                            </td>
                            <td class="total-value">
                                <div id="total">
                                    <input type="number" id="total_iva" name="total_iva" placeholder="$0.00" disabled readonly />
                                </div>
                            </td>
                            <td  class="blank"></td>
                            <td  class="blank"></td>
                        </tr>

                        <tr>


                            <td  class="item-name">
                                <strong>Total</strong>
                            </td>

                            <td class="total-value balance">
                                <div class="due">
                                    <input type="number" id="total_total" name="total_total" placeholder="$0.00" disabled readonly>
                                </div>
                            </td>
                            <td  class="blank"></td>
                            <td  class="blank"></td>
                        </tr>

			</tbody>
		</table>
		<section class="pagos2">
			<p>
				Tiempo de entrega: <span>
					<input type="number" id="dias" name="dias" placeholder="12" />
					días hábiles después del anticipo.</span>
			</p>
			<p>
				L.A.B.: <span>Cuautla, Morelos.</span>
			</p>
			<p>
				Condiciones de pago: $
				<input type="number" id="fifty" name="fifty" readonly />
				<select name="porcentaje" id="porcentaje" required>
					<option value="0" selected>0</option>
					<option value="0.2">20 </option>
					<option value="0.3">30 </option>
					<option value="0.4">40 </option>
					<option value="0.5">50 </option>
					<option value="0.6">60 </option>
					<option value="0.7">70 </option>
				</select>
				% de anticipo y  $
				<input type="number" class="fifty2" id="fifty2" name="fifty2" readonly />
				<input type="number" id="porcentaje2" name="porcentaje2" readonly />
				% contra entrega.</span>
			</p>
		</section>
					</div>
				</div>
				<!-- <input type="submit" value="Enviar" name="Enviar" id="enviar" class="btn btn-success btn-block"/> -->
                <button onClick='envioPdf(<?php echo $_GET['id']; ?>);' class="btn btn-success btn-block">Enviar</button>

				<div class="alert-material-amber"></div>
				<div class="row">
					<footer class="titulo">
						<p>
							Calle Calvario s/n Esq. Av. Insurgentes, Col. Guadalupe Victoria C.P.62746
						</p>
						<p>
							01 (735) 353 52 50 - 353 31 27-353 8577
						</p>
						<p>
							www.trapmsa.com.mx
						</p>
					</footer>
				</div>
                <section id="prueba"></section>
		</div>
    </div>

		<!-- </form> -->

		<label for="cosa" class="label-danger"></label>
	</body>

	<script src="js/jquery-1.8.3.min.js"></script>
	<script src="js/script.js"></script>
	<script src="js/enviar.js"> </script>
    <script type="text/javascript" src="js/pasarPdf.js"></script>


</html>