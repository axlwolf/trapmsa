function envioPdf(id_coti) {

	var fecha=$("input#fecha").val()
	var contacto=$("input#contacto").val()
	var email=$("input#email").val()
	var tipoVolteo=$("select#volteo").val()

	var titulo="<h3 style='color: white; background-color: #333333; text-align: center;'>TRANSFORMADORA DE PRODUCTOS METÁLICOS S.A. DE C.V.</h3>"

	if (fecha=='' || contacto=='' || email=='') {
		alert("Acomplete Los Campos Necesitados!!!")
	}else{
		var id=id_coti;

		//precio vehiculos
		var cantidad_precio=$("input#cantidad_precio").val()
		var precio_veh=$("input#precio_veh").val()
		var total_v=$("input#total_v").val()
		var deslizable=$("input.deslizable").val()
		if (deslizable==' ') {
			deslizable='';
		}else{
			deslizable=deslizable;
		}

		var coti=$("input#folio").val()
		var neumatica=$("input#hendrix").val()
		var eje=$("input#ejes2").val()
		var frenos=$(".abs").text()
		var rines=$("input#rines").val()
		var llantas=$("input#no_llantas").val()
		var llanMarc=$("select#llantas").val()
		
		//Seleccionamos los selects
		var volteo=$("#volteo").val()
		var modelo=$("#modelo").val()
		var dates=$("#dates").val()
		var largo=$("#largo").val()
		var largo_e=$("#largo").text()
		var ancho=$("#ancho_ext").text()
		var alto=$("#alto").val()
		var ejes=$("#ejes").val()

		//totales
		var precio_cotiza=$("input#precio_cotiza").val()
		var total_opcionales=$("input#total_opcionales").val()
		var total_iva=$("input#total_iva").val()
		var total_total=$("input#total_total").val()

		precio_cotiza=number_format(precio_cotiza, 2, '.', ',' )
		total_opcionales=number_format(total_opcionales, 2, '.', ',' )
		total_iva=number_format(total_iva, 2, '.', ',' )
		total_total=number_format(total_total, 2, '.', ',' )

		var dias=$("input#dias").val()
		var fifty=$("input#fifty").val()
		var porcentaje=($("select#porcentaje").val() * 100)
		var fifty2=$("input#fifty2").val()
		var porcentaje2=$("input#porcentaje2").val()

		fifty=number_format(fifty, 2, '.', ',' )
		fifty2=number_format(fifty2, 2, '.', ',' )

		var rinesVolteo=$('select#rines-volteo').val();
		var marck=$('select.marck').val()

		if (id==1) {//plantilla pdf volteo

			/*var largo=$("th#largo").val()*/
		/*var html=volteo+'-'+modelo+'-'+dates+'-'+largo+'-'+ancho+'-'+alto+'-'+ejes*/
		
		if (volteo=='VOLTEO HARDOX 24mts3' || volteo=='VOLTEO HARDOX 30mts3') {
			$(".delete-cost").remove()
		}
		var html='<nav><strong>FECHA: </strong><label>'+fecha+'</label><br>'+'<strong>CONTACTO: </strong><label>'+contacto+'</label><br><nav>'
		html +='<br><center><nav id="coti"><strong>COTIZACIÓN: </strong>'+'<label>'+coti+'</label><br/><small>De antemano agradecemos tu preferencia y le presento la cotización de la unidad posteriormente descrita.</small></nav></center>'
		html +='<table id="tabla1">'+
				'<tr>'+
				'<th>TIPO</th>'+
				'<th>MODELO</th>'+
				'<th>AÑO</th>'+
				'<th>LARGO EXT</th>'+
				'<th>ANCHO EXT</th>'+
				'<th>ALTO T</th>'+
				'<th>EJES</th>'+
				'</tr>'+
				'<tr>'+
				'<td>'+volteo+'</td>'+
				'<td>'+modelo+'</td>'+
				'<td>'+dates+'</td>'+
				'<td>'+largo+'</td>'+
				'<td>'+ancho+'</td>'+
				'<td>'+alto+'</td>'+
				'<td>'+ejes+'</td>'+
				'</tr>'+
				'</table><br/>'
		html +=$("section.estructura").html()
		html +='<h4><strong>SUSPENSIÓN</strong></h4>'+
		'<strong>Suspensión: </strong><label>Neumática '+neumatica+' HT-300 de 30,000lbs.</label><br><br>'+
		'<strong>Ejes: </strong><label>('+eje+')'+' En espesor de 3/4, capacidad de 30,000lbs.</label><br><br>'+
		'<strong>Frenos: </strong><label>'+frenos+'</label><br><br>'+
		'<strong>Rines: </strong><label>('+rines+')'+' '+rinesVolteo+'</label><br><br>'+
		'<strong>Llantas: </strong><label>('+llantas+')'+' '+marck+' '+llanMarc+'.</label><br><br>'+
		'<strong>Patines: </strong><label>Un juego mecánicos de 2 velocidades marca HOLLAND.</label><br>'+
		'<br/><strong>Porta llantas: </strong><label>Tipo canastilla.</label><br><br>'+
		'<strong> Retráctil: </strong><label>(1) Instalado en un eje.</label><br>'
		html +=$(".luces_pintura").html()
		/*$("section#prueba").html(html)*/
		}else if (id==2) {//platilla pdf tolva
			var domo=$("select#domos").val()
			var html='<nav><strong>FECHA: </strong><label>'+fecha+'</label><br>'+'<strong>CONTACTO: </strong><label>'+contacto+'</label><br><nav>'
		html +='<br><center><nav id="coti"><strong>COTIZACIÓN: </strong>'+'<label>'+coti+'</label><br/><small>De antemano agradecemos tu preferencia y le presento la cotización de la unidad posteriormente descrita.</small></nav></center>'
		html +='<table id="tabla1">'+
				'<tr>'+
				'<th>TIPO</th>'+
				'<th>MODELO</th>'+
				'<th>AÑO</th>'+
				'<th>LARGO EXT</th>'+
				'<th>ANCHO EXT</th>'+
				'<th>ALTO T</th>'+
				'<th>EJES</th>'+
				'</tr>'+
				'<tr>'+
				'<td>'+volteo+'</td>'+
				'<td>'+modelo+'</td>'+
				'<td>'+dates+'</td>'+
				'<td>'+largo_e+'</td>'+
				'<td>'+ancho+'</td>'+
				'<td>'+alto+'</td>'+
				'<td>'+ejes+'</td>'+
				'</tr>'+
				'</table><br/>'
		//html +=$("section.estructura").html()
		html +='<h4><strong>ESTRUCTURA</strong></h4>'+
		'<strong>Cuerpo: </strong><label>En lámina de acero al carbón cal.10.</label><br><br>'+
		'<strong>Cerchas: </strong><label>Canal comercial en acero al carbón en cal.3/16".</label><br><br>'+
		'<strong>Bastidor: </strong><label>En placa de alta resistencia cal.1/4".</label><br><br>'+
		'<strong>Acoplador superior: </strong><label>Fijo, soldado y atornillado entre las vigas principales del bastidor frontal, con placa de 3/8" en acero de alta resistencia, con perno rey HOLLAND.</label><br><br>'+
		'<strong>Domos: </strong><label>('+domo+')'+' En aluminio de 20" de diámetro con herrajes de sujeción.</label><br><br>'+
		'<strong>Manómetros: </strong><label> (2) De glicerina con 4" de caratula.</label><br><br>'+
		'<strong>Línea de alimentación: </strong><label>Tubería de 3" en acero negro ced.40.</label><br><br>'+
		'<strong>Línea de descarga: </strong><label>Tubería de 4" en acero negro ced.40.</label><br><br>'
		'<strong>Válvulas de alivio: </strong><label>(3) En línea de alimentación al tanque.</label><br><br>'
		'<strong>Válvula check: </strong><label>Instalada en línea de alimentación. "T" Descarga de material: Fabricada en aluminio de bajo perfil con entrada de 6" y descarga para tubo de 4"; con válvulas de mariposa de 6" para control de la descarga.</label><br><br>'
		html +='<h4><strong>SUSPENSIÓN</strong></h4>'+
		'<strong>Suspensión: </strong><label>Neumática '+neumatica+' HT-300 de 30,000lbs.</label><br><br>'+
		'<strong>Ejes: </strong><label>(2) Tubular redondo de espesor en 3/4 30,000 LBS.</label><br><br>'+
		'<strong>Frenos: </strong><label>'+frenos+'</label><br><br>'+
		'<strong>Rines: </strong><label>('+rines+')'+' '+rinesVolteo+'</label><br><br>'+
		'<strong>Llantas: </strong><label>('+llantas+')'+' '+marck+' '+llanMarc+'.</label><br><br>'+
		'<strong>Patines: </strong><label>Un juego; mecánico de 2 velocidades marca HOLLAND.</label><br><br><br/>'+
		'<strong>Porta llantas: </strong><label>Tipo canastilla.</label><br><br>'+
		'<strong>Fenders: </strong><label>De aluminio en la parte trasera y delantera del tanque.</label><br><br>'
		html +=$(".luces_pintura").html()
		}else if (id==3) {//platilla pdf dolly
			var html='<nav><strong>FECHA: </strong><label>'+fecha+'</label><br>'+'<strong>CONTACTO: </strong><label>'+contacto+'</label><br><nav>'
		html +='<br><center><nav id="coti"><strong>COTIZACIÓN: </strong>'+'<label>'+coti+'</label><br/><small>De antemano agradecemos tu preferencia y le presento la cotización de la unidad posteriormente descrita.</small></nav></center>'
		html +='<table id="tabla1">'+
				'<tr>'+
				'<th>TIPO</th>'+
				'<th>MODELO</th>'+
				'<th>AÑO</th>'+
				'<th>LARGO EXT</th>'+
				'<th>ANCHO EXT</th>'+
				'<th>ALTO T</th>'+
				'<th>EJES</th>'+
				'</tr>'+
				'<tr>'+
				'<td>'+volteo+'</td>'+
				'<td>'+modelo+'</td>'+
				'<td>'+dates+'</td>'+
				'<td>'+largo_e+'</td>'+
				'<td>'+ancho+'</td>'+
				'<td>'+alto+'</td>'+
				'<td>'+ejes+'</td>'+
				'</tr>'+
				'</table><br/>'
		html +=$("section.estructura").html()
		html +='<h4><strong>SUSPENSIÓN</strong></h4>'+
		'<strong>Suspensión: </strong><label>Neumática '+neumatica+' HT-300 de 30,000lbs.</label><br><br>'+
		'<strong>Ejes: </strong>(2) Tubular redondo de espesor en 3/4 30,000 LBS.</label><br><br>'+
		'<strong>Sistema de frenos: </strong><label>ABS 2S/1M.</label><br><br>'+
		'<strong>Lanza: </strong><label>Marca Holland o Premier, incluyendo tiro con bisagras.</label><br><br>'+
		'<strong>Quinta rueda: </strong><label>Marca HOLLAND o JOST.</label><br><br>'+
		'<strong>Cadenas y argollas de seguridad</strong><br/><br/>'+
		'<strong>Rines: </strong><label>('+rines+')'+' '+rinesVolteo+'</label><br><br>'+
		'<strong>Llantas: </strong><label>(8)'+' '+marck+' '+llanMarc+'.</label><br><br>'
		html +=$(".luces_pintura").html()

		}else if (id==4) {//platilla pdf caja seca
			var html='<nav><strong>FECHA: </strong><label>'+fecha+'</label><br>'+'<strong>CONTACTO: </strong><label>'+contacto+'</label><br><nav>'
		html +='<br><center><nav id="coti"><strong>COTIZACIÓN: </strong>'+'<label>'+coti+'</label><br/><small>De antemano agradecemos tu preferencia y le presento la cotización de la unidad posteriormente descrita.</small></nav></center>'
		html +='<table id="tabla1">'+
				'<tr>'+
				'<th>TIPO</th>'+
				'<th>MODELO</th>'+
				'<th>AÑO</th>'+
				'<th>LARGO EXT</th>'+
				'<th>ANCHO EXT</th>'+
				'<th>ALTO T</th>'+
				'<th>EJES</th>'+
				'</tr>'+
				'<tr>'+
				'<td>'+volteo+'</td>'+
				'<td>'+modelo+'</td>'+
				'<td>'+dates+'</td>'+
				'<td>'+largo_e+'</td>'+
				'<td>'+ancho+'</td>'+
				'<td>'+alto+'</td>'+
				'<td>'+ejes+'</td>'+
				'</tr>'+
				'</table>'
		html +=$("section.estructura").html()
		html +='<h4><strong>SUSPENSIÓN</strong></h4>'+
		'<strong>Suspensión: </strong>Neumática '+neumatica+' HT-300 de 30,000lbs '+deslizable+'.<br><br>'+
		'<strong>Ejes: </strong>(2) Tubular redondo de espesor en 3/4 30,000 LBS.<br><br>'+
		'<strong>Frenos: </strong>'+frenos+' <br><br>'+
		'<strong>Rines: </strong><label>('+rines+')'+' '+rinesVolteo+'</label><br><br>'+
		'<strong>Llantas: </strong><label>('+llantas+')'+' '+marck+' '+llanMarc+'.</label><br><br>'+
		'<strong>Patines: </strong>Un juego; mecánico de 2 velocidades marca HOLLAND.'
		html +=$(".luces_pintura").html()
		
		}else if (id==5) {//platilla pdf tolva

			var frenos=$("#plata-fre").text()
			var html='<nav><strong>FECHA: </strong><label>'+fecha+'</label><br>'+'<strong>CONTACTO: </strong><label>'+contacto+'</label><br><nav>'
		html +='<br><center><nav id="coti"><strong>COTIZACIÓN: </strong>'+'<label>'+coti+'</label><br/><small>De antemano agradecemos tu preferencia y le presento la cotización de la unidad posteriormente descrita.</small></nav></center>'
		html +='<table id="tabla1">'+
				'<tr>'+
				'<th>TIPO</th>'+
				'<th>MODELO</th>'+
				'<th>AÑO</th>'+
				'<th>LARGO EXT</th>'+
				'<th>ANCHO EXT</th>'+
				'<th>ALTO T</th>'+
				'<th>EJES</th>'+
				'</tr>'+
				'<tr>'+
				'<td>'+volteo+'</td>'+
				'<td>'+modelo+'</td>'+
				'<td>'+dates+'</td>'+
				'<td>'+largo+'</td>'+
				'<td>'+ancho+'</td>'+
				'<td>'+alto+'</td>'+
				'<td>'+ejes+'</td>'+
				'</tr>'+
				'</table><br/>'
		html +=$("section.estructura").html()
		html +='<h4><strong>SUSPENSIÓN</strong></h4>'+
		'<strong>Suspensión: </strong><label>Neumática '+neumatica+' HT-300 de 30,000lbs.</label><br><br>'+
		'<strong>Ejes: </strong><label>('+eje+')'+' En espesor de 3/4, capacidad de 30,000lbs.</label><br><br>'+
		'<strong>Frenos: </strong><label>'+frenos+'</label><br><br>'+
		'<strong>Rines: </strong><label>('+rines+')'+' '+rinesVolteo+'</label><br><br>'+
		'<strong>Llantas: </strong><label>('+llantas+')'+' '+marck+' '+llanMarc+'.</label><br><br>'+
		'<strong>Patines: </strong><label>Un juego mecánicos de 2 velocidades marca HOLLAND.</label><br><br>'+
		'<strong>Porta llantas: </strong><label>Tipo canastilla</label><br><br><br/><br/><br/><br/>'
		html +=$(".luces_pintura").html()
		}else if (id==6) {//platilla pdf chasis r2
			var frenos=$("#plata-fre").text()
			var html='<nav><strong>FECHA: </strong><label>'+fecha+'</label><br>'+'<strong>CONTACTO: </strong><label>'+contacto+'</label><br><nav>'
		html +='<br><center><nav id="coti"><strong>COTIZACIÓN: </strong>'+'<label>'+coti+'</label><br/><small>De antemano agradecemos tu preferencia y le presento la cotización de la unidad posteriormente descrita.</small></nav></center>'
		html +='<table id="tabla1">'+
				'<tr>'+
				'<th>TIPO</th>'+
				'<th>MODELO</th>'+
				'<th>AÑO</th>'+
				'<th>LARGO EXT</th>'+
				'<th>ANCHO EXT</th>'+
				'<th>ALTO T</th>'+
				'<th>EJES</th>'+
				'</tr>'+
				'<tr>'+
				'<td>'+volteo+'</td>'+
				'<td>'+modelo+'</td>'+
				'<td>'+dates+'</td>'+
				'<td>'+largo_e+'</td>'+
				'<td>'+ancho+'</td>'+
				'<td>'+alto+'</td>'+
				'<td>'+ejes+'</td>'+
				'</tr>'+
				'</table>'
		html +=$("section.estructura").html()
		html +='<h4><strong>SUSPENSIÓN</strong></h4>'+
		'<strong>Suspensión: </strong>Neumática '+neumatica+' HT-300 de 30,000lbs.<br><br>'+
		'<strong>Ejes: </strong>(2) Tubular redondo de espesor en 3/4, capacidad de 30,000 LBS.<br><br>'+
		'<strong>Frenos: </strong>'+frenos+' <br><br>'+
		'<strong>Rines: </strong>('+rines+')'+' Rhims unimont 22.5. <br><br>'+
		'<strong>Llantas: </strong>('+llantas+')'+' 11R22.5. marca '+llanMarc+' <br><br>'+
		'<strong>Patines: </strong>Un juego; mecánico de 2 velocidades marca HOLLAND. <br><br>'+
		'<strong>Porta llantas: </strong>Tipo canastilla.<br><br>'+
		'<strong>Gancho: </strong>De arrastre marca HOLLAND o PREMIER.'
		html +=$(".luces_pintura").html()
		}else if (id==7) {//platilla pdf chasis ex
			var frenos=$("#plata-fre").text()
			var html='<nav><strong>FECHA: </strong><label>'+fecha+'</label><br>'+'<strong>CONTACTO: </strong><label>'+contacto+'</label><br><nav>'
		html +='<br><center><nav id="coti"><strong>COTIZACIÓN: </strong>'+'<label>'+coti+'</label><br/><small>De antemano agradecemos tu preferencia y le presento la cotización de la unidad posteriormente descrita.</small></nav></center>'
		html +='<table id="tabla1">'+
				'<tr>'+
				'<th>TIPO</th>'+
				'<th>MODELO</th>'+
				'<th>AÑO</th>'+
				'<th>LARGO EXT</th>'+
				'<th>ANCHO EXT</th>'+
				'<th>ALTO T</th>'+
				'<th>EJES</th>'+
				'</tr>'+
				'<tr>'+
				'<td>'+volteo+'</td>'+
				'<td>'+modelo+'</td>'+
				'<td>'+dates+'</td>'+
				'<td>'+largo_e+'</td>'+
				'<td>'+ancho+'</td>'+
				'<td>'+alto+'</td>'+
				'<td>'+ejes+'</td>'+
				'</tr>'+
				'</table>'
		html +=$("section.estructura").html()
		html +='<h4><strong>SUSPENSIÓN</strong></h4>'+
		'<strong>Suspensión: </strong>Neumática '+neumatica+' HT-300 de 30,000lbs.<br><br>'+
		'<strong>Ejes: </strong>(2) Tubular redondo de espesor en 3/4 30,000 LBS.<br><br>'+
		'<strong>Frenos: </strong>'+frenos+' <br><br>'+
		'<strong>Rines: </strong>('+rines+')'+' Rhims unimont 22.5. <br><br>'+
		'<strong>Llantas: </strong>('+llantas+')'+' 11R22.5. marca '+llanMarc+' <br><br>'+
		'<strong>Patines: </strong>Un juego; mecánico de 2 velocidades marca HOLLAND. <br><br>'+
		'<strong>Porta llantas: </strong>Tipo canastilla.<br><br>'+
		'<strong>Gancho: </strong>De arrastre marca HOLLAND o PREMIER. <br><br><br><br><br/>'
		html +=$(".luces_pintura").html()
		}else if (id==8) {//platilla pdf plat mult.
			var frenos=$("#plata-fre").text()
			var html='<nav><strong>FECHA: </strong><label>'+fecha+'</label><br>'+'<strong>CONTACTO: </strong><label>'+contacto+'</label><br><nav>'
		html +='<br><center><nav id="coti"><strong>COTIZACIÓN: </strong>'+'<label>'+coti+'</label><br/><small>De antemano agradecemos tu preferencia y le presento la cotización de la unidad posteriormente descrita.</small></nav></center>'
		html +='<table id="tabla1">'+
				'<tr>'+
				'<th>TIPO</th>'+
				'<th>MODELO</th>'+
				'<th>AÑO</th>'+
				'<th>LARGO EXT</th>'+
				'<th>ANCHO EXT</th>'+
				'<th>ALTO T</th>'+
				'<th>EJES</th>'+
				'</tr>'+
				'<tr>'+
				'<td>'+volteo+'</td>'+
				'<td>'+modelo+'</td>'+
				'<td>'+dates+'</td>'+
				'<td>'+largo_e+'</td>'+
				'<td>'+ancho+'</td>'+
				'<td>'+alto+'</td>'+
				'<td>'+ejes+'</td>'+
				'</tr>'+
				'</table>'
		html +=$("section.estructura").html()
		html +='<h4><strong>SUSPENSIÓN</strong></h4>'+
		'<strong>Suspensión: </strong>Neumática '+neumatica+' HT-300 de 30,000lbs.<br><br>'+
		'<strong>Ejes: </strong>(2) Tubular redondo de espesor en 3/4 30,000 LBS.<br><br>'+
		'<strong>Frenos: </strong>'+frenos+' <br><br>'+
		'<strong>Rines: </strong>('+rines+')'+' Rhims unimont 22.5. <br><br>'+
		'<strong>Llantas: </strong>('+llantas+')'+' 11R22.5. marca '+llanMarc+' <br><br>'+
		'<strong>Patines: </strong>Un juego; mecánico de 2 velocidades marca HOLLAND. <br><br>'+
		'<strong>Porta llantas: </strong>Tipo canastilla.<br><br>'+
		'<strong>Gancho: </strong>De arrastre marca HOLLAND o PREMIER.<br><br><br><br>'
		html +=$(".luces_pintura").html()
		}else if (id==9) {//platilla pdf tolva

			var frenos=$(".abs").text()
			var larg=$("#cambio-volt").text()
			var alto=$("#cambio2-volt").text()
			var html='<nav><strong>FECHA: </strong><label>'+fecha+'</label><br>'+'<strong>CONTACTO: </strong><label>'+contacto+'</label><br><nav>'
		html +='<br><center><nav id="coti"><strong>COTIZACIÓN: </strong>'+'<label>'+coti+'</label><br/><small>De antemano agradecemos tu preferencia y le presento la cotización de la unidad posteriormente descrita.</small></nav></center>'
		html +='<table id="tabla1">'+
				'<tr>'+
				'<th>TIPO</th>'+
				'<th>MODELO</th>'+
				'<th>AÑO</th>'+
				'<th>LARGO EXT</th>'+
				'<th>ANCHO EXT</th>'+
				'<th>ALTO T</th>'+
				'<th>EJES</th>'+
				'</tr>'+
				'<tr>'+
				'<td>'+volteo+'</td>'+
				'<td>'+modelo+'</td>'+
				'<td>'+dates+'</td>'+
				'<td>'+larg+'</td>'+
				'<td>'+ancho+'</td>'+
				'<td>'+alto+'</td>'+
				'<td>'+ejes+'</td>'+
				'</tr>'+
				'</table>'
		html +=$("section.estructura").html()
		html +='<h4><strong>SUSPENSIÓN</strong></h4>'+
		'<strong>Suspensión: </strong>Neumática '+neumatica+' HT-300 de 30,000lbs.<br><br>'+
		'<strong>Ejes: </strong>(2) Tubular redondo de espesor en 3/4 30,000 LBS.<br><br>'+
		'<strong>Frenos: </strong>'+frenos+' <br><br>'+
		'<strong>Rines: </strong><label>('+rines+')'+' '+rinesVolteo+'</label><br><br>'+
		'<strong>Llantas: </strong><label>('+llantas+')'+' '+marck+' '+llanMarc+'.</label><br><br>'+
		'<strong>Patines: </strong>Un juego; mecánico de 2 velocidades marca HOLLAND. <br><br>'+
		'<strong>Porta llantas: </strong>Tipo canastilla.<br><br>'+
		'<strong>Retráctil: </strong>(1) Instalado en un eje.'
		html +=$(".luces_pintura").html()
		}else if (id==10) {//plantilla pdf volteo

		var html='<nav><strong>FECHA: </strong><label>'+fecha+'</label><br>'+'<strong>CONTACTO: </strong><label>'+contacto+'</label><br><nav>'
		html +='<br><center><nav id="coti"><strong>COTIZACIÓN: </strong>'+'<label>'+coti+'</label><br/><small>De antemano agradecemos tu preferencia y le presento la cotización de la unidad posteriormente descrita.</small></nav></center>'
		html +='<table id="tabla1">'+
				'<tr>'+
				'<th>TIPO</th>'+
				'<th>MODELO</th>'+
				'<th>AÑO</th>'+
				'<th>LARGO EXT</th>'+
				'<th>ANCHO EXT</th>'+
				'<th>ALTO T</th>'+
				'<th>EJES</th>'+
				'</tr>'+
				'<tr>'+
				'<td>'+volteo+'</td>'+
				'<td>'+modelo+'</td>'+
				'<td>'+dates+'</td>'+
				'<td>'+largo+'</td>'+
				'<td>'+ancho+'</td>'+
				'<td>'+alto+'</td>'+
				'<td>'+ejes+'</td>'+
				'</tr>'+
				'</table><br/>'
		html +=$("section.estructura").html()
		html +='<h4><strong>SUSPENSIÓN</strong></h4>'+
		'<strong>Suspensión: </strong><label>Neumática '+neumatica+' HT-300 de 30,000lbs.</label><br><br>'+
		'<strong>Ejes: </strong><label>('+eje+')'+' En espesor de 3/4, capacidad de 30,000lbs.</label><br><br>'+
		'<strong>Frenos: </strong><label>'+frenos+'</label><br><br>'+
		'<strong>Rines: </strong><label>('+rines+')'+' '+rinesVolteo+'</label><br><br>'+
		'<strong>Llantas: </strong><label>('+llantas+')'+' '+marck+' '+llanMarc+'.</label><br><br>'+
		'<strong>Patines: </strong><label>Un juego mecánicos de 2 velocidades marca HOLLAND.</label><br>'+
		'<br/><strong>Porta llantas: </strong><label>Tipo canastilla.</label><br><br>'
		html +=$(".luces_pintura").html()
		/*$("section#prueba").html(html)*/
		}else{
			alert("No Existe Plantilla!!!")
		}

		/*html +='<center><h4>PRECIO VEHICULO:</center><h4/><table id="tabla2">'+
		'<tr>'+
				'<th>PRECIO</th>'+
				'<th>IVA</th>'+
				'<th>TOTAL</th>'+
				'</tr>'+
				'<tr>'+
				'<td>'+cantidad_precio+'</td>'+
				'<td>'+precio_veh+'</td>'+
				'<td>'+total_v+'</td>'+
				'</tr>'+
				'</table><br/>'*/

		var html2 ='<table id="cuentas">'+
				'<tr>'+
				'<td class="titulos">PRECIO</td>'+
				'<td class="titulo2">$'+precio_cotiza+'</td>'+
				'</tr>'+
				'<tr>'+
				'<td class="titulos">TOTAL DE OPCIONALES</td>'+
				'<td class="titulo2">$'+total_opcionales+'</td>'+
				'</tr>'+
				'<tr>'+
				'<td class="titulos">TOTAL IVA</td>'+
				'<td class="titulo2">$'+total_iva+'</td>'+
				'</tr>'+
				'<tr>'+
				'<td class="titulos">TOTAL</td>'+
				'<td class="titulo2">$'+total_total+'</td>'+
				'</tr>'+
				'</table><br/>'

		html2 +='<table>'+
				'<tr>'+
				'<td class="titulos">TIEMPO DE ENTREGA</td>'+
				'<td class="titulo2">'+dias+' días hábiles después del anticipo.</td>'+
				'</tr>'+
				'<tr>'+
				'<td class="titulos">L.A.B.</td>'+
				'<td class="titulo2">Cuautla, Morelos.</td>'+
				'</tr>'+
				'<tr>'+
				'<td class="titulos">CONDICIONES DE PAGO</td>'+
				'<td class="titulo2">$'+fifty+' '+porcentaje+' % de anticipo | $ '+fifty2+' '+porcentaje2+' % contra entrega.</td>'+
				'</tr>'+
				'</table><br/>'
		
		/*html2 +='<nav id="coti2"><label>Tiempo de entrega: '+dias+' días hábiles después del anticipo.</label>'+
		'<br><label>L.A.B.: Cuautla, Morelos.</label>'+
		'<label>Condiciones de pago: $'+fifty+' '+porcentaje+' % de anticipo y $ '+fifty2+' '+porcentaje2+' % contra entrega.</label></nav><br/>'*/
		$.ajax({
				type: "POST",
				url: "subir-pdf/subir-pdf.php",
				data: {html:html,html2:html2,id:id,fecha:fecha,contacto:contacto,email:email,coti:coti,titulo:titulo,tipoVolteo:tipoVolteo},
				success: function(a){
					$("section#prueba").html(a).css({"background-color": "#FF8F32"})
					window.location.href = '../../cotizaciones.php';
				}
			});
		/*event.preventDefault();*/
	}
	
}

function number_format(number, decimals, decPoint, thousandsSep){
	decimals = decimals || 0;
	number = parseFloat(number);
 
	if(!decPoint || !thousandsSep){
		decPoint = '.';
		thousandsSep = ',';
	}
 
	var roundedNumber = Math.round( Math.abs( number ) * ('1e' + decimals) ) + '';
	var numbersString = decimals ? roundedNumber.slice(0, decimals * -1) : roundedNumber;
	var decimalsString = decimals ? roundedNumber.slice(decimals * -1) : '';
	var formattedNumber = "";
 
	while(numbersString.length > 3){
		formattedNumber += thousandsSep + numbersString.slice(-3)
		numbersString = numbersString.slice(0,-3);
	}
 
	return (number < 0 ? '-' : '') + numbersString + formattedNumber + (decimalsString ? (decPoint + decimalsString) : '');
}
 
//english format
//number_format( 1234.50, 2, '.', ',' ); // ~> "1,234.50" 