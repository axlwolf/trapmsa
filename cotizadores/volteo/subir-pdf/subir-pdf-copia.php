<?php
include "../../../Conexion.php";
require('attach_mailer_class.php');
require_once("dompdf_config.inc.php");
date_default_timezone_set("Mexico/General");
session_start();
ini_set('memory_limit', '-1');

$id_usuario  =  $_SESSION['id'];

if ($_POST) {

    $fecha=date("Y-m-d H:i:s");
    $contacto=$_POST['contacto'];
    $email=$_POST['email'];
    $folio=$_POST['coti'];

    $titulo=$_POST['titulo'];

    if ($_POST['id']==1) {
        $tipoCotizacion='Volteo';
        $rutaImage='volteo/1_opt.png';
    }elseif ($_POST['id']==2) {//pendiente en imagen
        $tipoCotizacion='Tolva';
        $rutaImage='tolva/tanquepresurizado_opt.png';
    }elseif ($_POST['id']==3) {
        $tipoCotizacion='Dolly';
        $rutaImage='dolly/dolly1_opt.png';
    }elseif ($_POST['id']==4) {//pendiente en imagen
        $tipoCotizacion='Caja Seca';
        $rutaImage='cajaSeca/Cajaseca_opt.png';
    }elseif ($_POST['id']==5) {//pendiente en imagen
        $tipoCotizacion='Plataforma';
        $rutaImage='plataforma/Plataforma_opt.png';
    }elseif ($_POST['id']==6) {//pendiente en imagen
        $tipoCotizacion='chasis-r20';
        $rutaImage='chasis-r20/CHASISPORT_opt.png';
    }elseif ($_POST['id']==7) {//pendiente en imagen
        $tipoCotizacion='chasis-x20';
        $rutaImage='chasis-x20/Chasis-porta-extendible_opt.png';
    }elseif ($_POST['id']==8) {//pendiente en imagen
        $tipoCotizacion='Multi-Plat';
        $rutaImage='multi-plat/caja-multimodal_opt.png';
    }elseif ($_POST['id']==9) {//pendiente en imagen
        $tipoCotizacion='Volteo-Plat';
        $rutaImage='volte-plat/multibox_opt.png';
    }elseif ($_POST['id']==10) {//pendiente en imagen
        $tipoCotizacion='Encortinado';
        $rutaImage='encortinado/encortinada_opt.png';
    }
        $ano=date("Y");
        $dompdf = new DOMPDF();

        $content = $_POST['html'];
        $content2 = $_POST['html2'];

        $html = '<!DOCTYPE HTML><html lang="es"><head><meta charset="utf-8" /><link rel="stylesheet" type="text/css" href="pdf2.css" /></head>'. 
        '<body>'.
        '<header>'.
        '<br><center><img src="img-new/BANNER3.png"></center>'.
        '</header>'.
        '<section id="prueba">'.
        utf8_decode($content).
        '<br><center><img src="img-new/'.$rutaImage.'" /></center><br>'.
        utf8_decode($content2).
        '</section><br/>'. 
        '<center><img src="img-new/direccion.png" /></center><br/>'.
        '</body>'.
        '</html>';

        $html2 = '<!DOCTYPE HTML><html lang="es"><head><meta charset="utf-8" /></head>'. 
        '<body style="border: 1px solid black;">'.
        $titulo.
        '<h5 style="color: white; background-color: #333333; text-align: center;">Nuestro distintivo siempre será la innovación y calidad para nuestros clientes.</h5>'.
        '<center><section id="arriba" style="display: inline-block; border-right: 2px solid #333333; border-bottom: 2px solid #333333; vertical-align: top; width:40%;">'.
        '<a href="http://www.ventas@trapmsa.com.mx">ventas@trapmsa.com.mx</a>'.
        '<h4>MATRIZ</h4>'.
        '<label>Calle Calvario s/n Esq. Av. Insurgentes, Col. Guadalupe Victoria C.P.62746.<br/>Cuautla, Morelos</label><br/>'.
        'Tel.+52 (735) 35 35 250<br/>+52 (735) 353 31 27'.
        '</section>'.
        '<section id="abajo" style="display: inline-block; vertical-align: top; border-bottom: 2px solid #333333; width:40%;">'.
        '<a href="http://www.trapmsa.com.mx">www.trapmsa.com.mx</a>'.
        '<h4>SUCURSAL</h4>'.
        '<label>Carr. México - Cuautla Km. 97.6 No.22, Col. Lázaro Cárdenas.<br/>Cuautla, Morelos</label><br/>'.
        'Tel.+52 (735) 353 85 77<br/><br/>'.
        '</section></center>'.
        '<h4 style="color: white; background-color: #333333; text-align: center;">¡MUCHAS GRACIAS POR TU PREFERENCIA!</h4>'.
        '</body>'.
        '</html>';


        //echo $html;
        $dompdf->load_html($html);
        $dompdf->render();


        $file="../pdf/cotizacion ".$folio .'.pdf';
        $output = $dompdf->output();
        if (file_put_contents($file, $output)) {
               /* echo "Archivo Creado";*/
                $from = "ventas@cotizaciones-trapmsa.com";
                $to = $email;


                        $test = new attach_mailer($name = "Cotizaciones Trapmsa", $from = $from, $to = $to, $cc = "ventas@trapmsa.com.mx", $bcc = "contacto@cotizaciones-trapmsa.com", $subject = "Cotizacion", $body = $html2);
                        $test->create_attachment_part($file); 
                        $test->process_mail();

                        $query = "INSERT INTO cotizaciones(fecha, cliente, email, folio_cotizacion, tipo_cotiza, id_usuario) VALUES ('$fecha', '$contacto','$email', '$folio','$tipoCotizacion','$id_usuario')";
                        #Resultado
                        $resultado = $conexion -> query($query) or die($conexion -> error . __LINE__);
                        echo $test->get_msg_str();

                        //echo $test->get_msg_str();
        }else{
                echo "Archivo no Creado";
        }
}else{
        echo "No existio envio";
}

/*$dompdf->stream("resultado.pdf"); crea el archivo este se pone cuando no tenemos la direccion para save
ba abajo de la funcion dompdf->render()*/
?>